/*====================================================================

			       照応解析

                                         Ryohei Sasano 2007. 8. 27

    $Id: anaphora.c,v 1.65 2011/09/15 05:30:09 ryohei Exp $
====================================================================*/

#include "knp.h"
#include "anaphora.h"

/* 位置カテゴリ(主節や用言であるか等は無視)    */
#define	LOC_SELF             0 /* 自分自身     */
#define	LOC_PARENT           1 /* 親           */
#define	LOC_CHILD            2 /* 子供         */
#define LOC_PARA_PARENT      3 /* 並列(親側)   */
#define	LOC_PARA_CHILD       4 /* 並列(子側)   */
#define	LOC_PARENT_N_PARENT  5 /* 親体言の親   */
#define	LOC_PARENT_V_PARENT  6 /* 親用言の親   */
#define	LOC_OTHERS_BEFORE    7 /* その他(前)   */
#define	LOC_OTHERS_AFTER     8 /* その他(後)   */
#define	LOC_OTHERS_THEME     9 /* その他(主題) */
#define INITIAL_SCORE        -9999

/* clear_contextされた時点での文数、ENTITY数を記録 */
int base_sentence_num = 0;
int base_entity_num = 0;

/* 位置カテゴリを保持 */
int loc_category[BNST_MAX];

/* 直近で出現したX格の出現位置 */
int nearest_case_sen[4];
int nearest_case_tag[4];

/* 解析結果を保持するためのENTITY_CASE_MGR
   先頭のCASE_CANDIDATE_MAX個に照応解析用格解析の結果の上位の保持、
   次のELLIPSIS_RESULT_MAX個には省略解析結果のベスト解の保持、
   最後の1個は現在の解析結果の保持に使用する */
CF_TAG_MGR work_ctm[CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX + 1];

/* 省略解析の対象とする格のリスト */
char *ELLIPSIS_CASE_LIST_VERB[] = {"ガ", "ヲ", "ニ", "\0"};
char *ELLIPSIS_CASE_LIST_NOUN[] = {"ノ", "ノ", "ノ？", "\0"};
char **ELLIPSIS_CASE_LIST = ELLIPSIS_CASE_LIST_VERB;

/* Salience score ranking を作る際に使用する構造体 */
STRUCT_FOR_QSORT struct_for_qsort[ENTITY_MAX];

/*==================================================================*/
	      int cmp_sfq(const void *a, const void *b)
/*==================================================================*/
{
    /* Salience score ranking を作る際に使用する比較関数 */
    return ((*((STRUCT_FOR_QSORT *)b)).score - (*((STRUCT_FOR_QSORT *)a)).score > 0) ? 1 : -1;
}

/*==================================================================*/
	   void clear_context(SENTENCE_DATA *sp, int init_flag)
/*==================================================================*/
{
    int i;

    if (OptAnaphora & OPT_PRINT_ENTITY) fprintf(Outfp, ";;\n;;CONTEXT INITIALIZED\n");
    for (i = 0; i < sp->Sen_num - 1; i++) ClearSentence(sentence_data + i);
    if (init_flag) {
	base_sentence_num = base_entity_num = 0;
	corefer_id = 0;
    }
    else {
	base_sentence_num += sp->Sen_num - 1;
	base_entity_num += entity_manager.num;
    }   
    sp->Sen_num = 1;
    entity_manager.num = 0;
}

/*==================================================================*/
	    int match_ellipsis_case(char *key, char **list)
/*==================================================================*/
{
    /* keyが省略対象格のいずれかとマッチするかどうかをチェック */
    int i;

    /* 引数がある場合はそのリストを、
       ない場合はELLIPSIS_CASE_LISTをチェックする */
    if (!list) list = ELLIPSIS_CASE_LIST;

    for (i = 0; *list[i]; i++) {
	if (!strcmp(key, list[i])) return TRUE;
    }
    return FALSE;
}

/*==================================================================*/
	       void assign_mrph_num(SENTENCE_DATA *sp)
/*==================================================================*/
{
    /* 文先頭からその形態素の終りまでの文字数を与える */
    int i, count = 0;

    for (i = 0; i < sp->Mrph_num; i++) {
	count += strlen((sp->mrph_data + i)->Goi2) / 2;
	(sp->mrph_data + i)->Num = count;
    }
}

/*==================================================================*/
	   TAG_DATA *substance_tag_ptr(TAG_DATA *tag_ptr)
/*==================================================================*/
{
    /* tag_ptrの実体を返す関数(並列構造への対処のため) */
    /* castすることによりbnst_ptrに対しても使用 */
    while (tag_ptr && tag_ptr->para_top_p) tag_ptr = tag_ptr->child[0];
    return tag_ptr;
}

/*==================================================================*/
int get_location(char *loc_name, int sent_num, char *kstr, MENTION *mention, int old_flag)
/*==================================================================*/
{
    /* 同一文の場合 */
    if (mention->sent_num == sent_num) {
	sprintf(loc_name, "%s-%c%s-C%d", kstr,
		(mention->type == '=') ? 'S' : 
		(mention->type == 'N') ? 'C' : mention->type, 
		old_flag ? "" : mention->cpp_string,
		loc_category[mention->tag_ptr->b_ptr->num]);
	return TRUE;
    }
    /* 先行文 */
    else if (sent_num - mention->sent_num > 0) {
	sprintf(loc_name, "%s-%c%s-B%d", kstr, 
		(mention->type == '=') ? 'S' : 
		(mention->type == 'N') ? 'C' : mention->type, 
		old_flag ? "" : mention->cpp_string,
		(sent_num - mention->sent_num <= 3 ) ? 
		sent_num - mention->sent_num : 0);
	return TRUE;
    }
    return FALSE;
}

/*==================================================================*/
int get_location_comp(char *loc_name, int sent_num, char *kstr, MENTION *mention)
/*==================================================================*/
{
    /* C[ガヲニ]-C[247]である場合、および、係先が判定詞など解析対象外の用言の場合は
       解析を行っていない箇所の解析結果が必要となるためここで生成する */
    char case_name[BYTES4CHAR + 1];

    /* 先行詞候補が係先を持っていて、
       かつ、係先の解析が行われていない場合(mention_mgr->num == 1)のみ処理の対象 */
    if (!((mention->type == '=' || mention->type == 'S') &&
	  mention->tag_ptr->b_ptr->parent &&
	  mention->tag_ptr->b_ptr->parent->tag_ptr->mention_mgr.num == 1)) return FALSE;

    /* 格がガ格、ヲ格、ニ格のいずれか */
    if (check_feature(mention->tag_ptr->f, "係:ガ格") ||
	check_feature(mention->tag_ptr->f, "係:未格") &&
	check_feature(mention->tag_ptr->f, "解析格:ガ")) {
	strcpy(case_name, "ガ");
    }
    else if (check_feature(mention->tag_ptr->f, "係:ヲ格") ||
	check_feature(mention->tag_ptr->f, "係:未格") &&
	check_feature(mention->tag_ptr->f, "解析格:ヲ")) {
	strcpy(case_name, "ヲ");
    }
    else if (check_feature(mention->tag_ptr->f, "係:ニ格") ||
	check_feature(mention->tag_ptr->f, "係:未格") &&
	check_feature(mention->tag_ptr->f, "解析格:ニ")) {
	strcpy(case_name, "ニ");
    }
    else {
	return FALSE;
    }

    /* 同一文 */
    if (mention->sent_num == sent_num &&
	sprintf(loc_name, "%s-C%s-C%d", kstr, case_name, 
		loc_category[mention->tag_ptr->b_ptr->parent->num])) {
	return TRUE;
    }

    return FALSE;
}

/*==================================================================*/
     void mark_loc_category(SENTENCE_DATA *sp, TAG_DATA *tag_ptr)
/*==================================================================*/
{
    /* 文節ごとに位置カテゴリを付与する */
    /* 格要素ではなく用言(名詞も含む)側に付与 */
    int i, j;
    BNST_DATA *bnst_ptr, *parent_ptr = NULL, *pparent_ptr = NULL;

    bnst_ptr = (BNST_DATA *)substance_tag_ptr((TAG_DATA *)tag_ptr->b_ptr);

    /* 初期化 */
    /* その他(前) */
    for (i = 0; i < bnst_ptr->num; i++) loc_category[i] = LOC_OTHERS_BEFORE;
    /* その他(後) */
    for (i = bnst_ptr->num + 1; i < sp->Bnst_num; i++) loc_category[i] = LOC_OTHERS_AFTER;
    loc_category[bnst_ptr->num] = LOC_SELF; /* 自分自身 */

    /* 自分が並列である場合 */    
    /* KNPの並列構造(半角数字は文節番号)                  */
    /*                      １と0<P>─┐　　　　　          */
    /*                      ２、1<P>─┤　　　　　          */
    /*             Ａと2<P>─┐ 　 　　│　　　 　           */
    /*             Ｂと3<P>─┤ 　 　　│　　　 　           */
    /*             Ｃ、4<P>─┤ 　 　　│　　　 　           */
    /* アと 5<P>─┐　   　　 │ 　 　　│　　 　　　         */
    /* イの10<P>-PARA9<P>-PARA8<P>─PARA6──┐　             */
    /*                                   関係。7          */
    /* 文節6,7のみpara_type=PARA_NIL、6,8,9がpara_top_p=1 */
    if (bnst_ptr->para_type == PARA_NORMAL) {
	for (i = 0; bnst_ptr->parent->child[i]; i++) {
	    if (bnst_ptr->parent->child[i]->para_type == PARA_NORMAL &&
		/* todo::とりあえず並列の並列は無視 */
		!bnst_ptr->parent->child[i]->para_top_p) {
		/* 並列(親側) */
		if (bnst_ptr->parent->child[i]->num > bnst_ptr->num)
		    loc_category[bnst_ptr->parent->child[i]->num] = LOC_PARA_PARENT;
		/* 並列(子供側) */
		else if (bnst_ptr->parent->child[i]->num < bnst_ptr->num)
		    loc_category[bnst_ptr->parent->child[i]->num] = LOC_PARA_CHILD;
	    }
	}
	/* 親を探索 */
	parent_ptr = bnst_ptr->parent;
	while (parent_ptr->para_top_p && parent_ptr->parent) parent_ptr = parent_ptr->parent;
	if (parent_ptr->para_top_p) parent_ptr = NULL;	
    }
    /* 自分が並列でない場合 */
    else if (bnst_ptr->parent) {
	parent_ptr = bnst_ptr->parent;
    }
    
    /* 親、親用言の親、親体言の親 */
    if (parent_ptr) {
	loc_category[parent_ptr->num] = LOC_PARENT; /* 親 */

	/* 親の親を探索 */
	if (parent_ptr->parent) {
	    pparent_ptr = parent_ptr->parent;
	    while (pparent_ptr->para_top_p && pparent_ptr->parent) pparent_ptr = pparent_ptr->parent;
	    if (pparent_ptr->para_top_p) pparent_ptr = NULL;
	}

	if (pparent_ptr) {
	    if (check_feature(parent_ptr->f, "用言"))
		loc_category[pparent_ptr->num] = LOC_PARENT_V_PARENT; /* 親用言の親 */
	    else
		loc_category[pparent_ptr->num] = LOC_PARENT_N_PARENT; /* 親体言の親 */
	}
    }	           	

    /* 子供 */
    for (i = 0; bnst_ptr->child[i]; i++) {
	/* 子が並列の場合(ex. 聞いていた) */
	/*   彼は──┐　　　　　　   　　　 */
	/*  食べながら、<P>─┐　　　  　　 */
	/*   彼女は──┐　　　│  　　　　　 */
	/*    飲みながら<P>─PARA──┐   　  */
	/*                   聞いていた。 */   
	if (bnst_ptr->child[i]->para_top_p) { 
	    for (j = 0; bnst_ptr->child[i]->child[j]; j++) {
		/* todo::とりあえず並列の並列は無視 */		
		if (!bnst_ptr->child[i]->child[j]->para_top_p)
		    loc_category[bnst_ptr->child[i]->child[j]->num] = LOC_CHILD; /* 子供 */
	    }
	}
	else {
	    loc_category[bnst_ptr->child[i]->num] = LOC_CHILD; /* 子供 */
	}
    }		    	   	   	    
    /* 自分が並列である場合(ex. 解決や) */
    /*     その──┐　　　　　　　　  　  */
    /*     早い──┤                      */
    /* 解決や<P>─┤　　　　　　　　  　  */
    /* 実現の<P>─PARA──┐　　　    　　  */
    /*                手伝いを──┐   　  */
    /*                          する。  */
    if (bnst_ptr->para_type == PARA_NORMAL) {
	for (i = 0; bnst_ptr->parent->child[i]; i++) {

	    /* todo::とりあえず並列の並列は無視 */		
	    if (bnst_ptr->parent->child[i]->para_type == PARA_NIL) {
		loc_category[bnst_ptr->parent->child[i]->num] = LOC_CHILD; /* 子供 */
	    }
	}
    }

    /* 自分自身を越えて係る"は" */
    for (i = 0; i < bnst_ptr->num; i++) {
	if ((sp->bnst_data[i].parent)->num &&
	    (sp->bnst_data[i].parent)->num > bnst_ptr->num &&
	    check_feature(sp->bnst_data[i].f, "ハ")) loc_category[i] = LOC_OTHERS_THEME;
    }

    if (OptDisplay == OPT_DEBUG) {
	for (i = 0; i < sp->Bnst_num; i++)
	    fprintf(Outfp, ";;LOC %d-%s target_bnst:%d-%d\n", bnst_ptr->num,
		   bnst_ptr->Jiritu_Go, i, loc_category[i]);
    }
    
    return;
}

/*==================================================================*/
     void mark_nearest_case(SENTENCE_DATA *sp, TAG_DATA *tag_ptr)
/*==================================================================*/
{
    int i, tag_num, sen_num, type;
    TAG_DATA *tptr;

    /* 初期化 */   
    tag_num = tag_ptr->num;
    sen_num = sp->Sen_num;
    for (i = 0; i < 4; i++) nearest_case_tag[i] = -1; 

    while (sen_num > 0) {
	while (tag_num > -1) {
	    type = -1; /* 0:ガ格, 1:ヲ格, 2:ニ格, 3:ハ格 */
	    tptr = substance_tag_ptr(((sentence_data + sen_num - 1))->tag_data + tag_num);
	    if (nearest_case_tag[0] == -1 && check_feature(tptr->f, "係:ガ格")) type = 0;
	    else if (nearest_case_tag[1] == -1 && check_feature(tptr->f, "係:ヲ格")) type = 1;
	    else if (nearest_case_tag[2] == -1 && check_feature(tptr->f, "係:ニ格")) type = 2;
	    else if (nearest_case_tag[3] == -1 && check_feature(tptr->f, "係:未格") && check_feature(tptr->f, "ハ")) type = 3;
	    if (type != -1) {
		nearest_case_sen[type] = sen_num;
		nearest_case_tag[type] = tag_num;
	    }
	    if (nearest_case_tag[0] != -1 && nearest_case_tag[1] != -1 && 
		nearest_case_tag[2] != -1 && nearest_case_tag[3] != -1) return;
	    tag_num--;
	}
	sen_num--;
	if (sen_num > 0) tag_num = (sentence_data + sen_num - 1)->Tag_num - 1;
    }
}

/*==================================================================*/
	       int check_analyze_tag(TAG_DATA *tag_ptr)
/*==================================================================*/
{
    /* 与えたられたtag_ptrが解析対象かどうかをチェック */

    /* 用言としての解析対象である場合:CF_PRED(=1)を返す */
    /* 名詞としての解析対象である場合:CF_NOUN(=2)を返す */
    /* それ以外の場合は0を返す */
    int i;
    BNST_DATA *bnst_ptr;

    /* 用言として解析する場合 */
    if ((OptEllipsis & OPT_ELLIPSIS) &&	check_feature(tag_ptr->f, "用言")) {

	/* 省略解析なし */
	if (!(OptAnaphora & OPT_ANAPHORA_ALL) &&	    
	    check_feature(tag_ptr->f, "省略解析なし")) return 0;

	/* 付属語は解析しない */
	if (check_feature(tag_ptr->mrph_ptr->f, "付属")) return 0;

	/* 単独の用言が『』で囲まれている場合は省略解析しない(暫定的) */
	if (check_feature(tag_ptr->f, "括弧始") &&
	    check_feature(tag_ptr->f, "括弧終")) return 0;

	/* サ変は文節主辞のみ対象 */
	if (check_feature(tag_ptr->f, "文節内") && 
	    check_feature(tag_ptr->f, "サ変")) return 0;

	return CF_PRED;
    }

    /* 名詞として解析する場合 */
    else if ((OptEllipsis & OPT_REL_NOUN) && check_feature(tag_ptr->f, "体言") &&
	     !check_feature(tag_ptr->f, "用言一部")) {
	
	/* 固有表現，共参照関係にある語等は対象外 */
	if (check_feature(tag_ptr->f, "NE") ||
	    check_feature(tag_ptr->f, "NE内") ||
	    check_feature(tag_ptr->f, "同格") ||
	    check_feature(tag_ptr->f, "共参照") ||
	    check_feature(tag_ptr->f, "共参照内")) return 0;
	
	/* 主辞以外は対象外 */
	if (check_feature(tag_ptr->f, "文節内")) return 0;

	/* 形副名詞は対象外 */
	if (check_feature(tag_ptr->f, "形副名詞")) return 0;

	bnst_ptr = (BNST_DATA *)substance_tag_ptr((TAG_DATA *)tag_ptr->b_ptr);

	/* 文末の連用修飾されている体言は除外 */
	if (check_feature(bnst_ptr->f, "文末") && !check_feature(bnst_ptr->f, "文頭") &&
	    bnst_ptr->child[0] && check_feature(bnst_ptr->child[0]->f, "係:連用")) return 0;
	return CF_NOUN; 
    }

    return 0;
}

/*==================================================================*/
int read_one_annotation(SENTENCE_DATA *sp, TAG_DATA *tag_ptr, char *token, int co_flag)
/*==================================================================*/
{
    /* 解析結果からMENTION、ENTITYを作成する */
    /* co_flagがある場合は"="のみを処理、ない場合は"="以外を処理 */
    char type, rel[SMALL_DATA_LEN], *cp, loc_name[SMALL_DATA_LEN];
    int i, j, tag_num, sent_num, bnst_num, diff_sen;
    TAG_DATA *parent_ptr;
    MENTION_MGR *mention_mgr = &(tag_ptr->mention_mgr);
    MENTION *mention_ptr = NULL;
    ENTITY *entity_ptr;
    
    if (!sscanf(token, "%[^/]/%c/%*[^/]/%d/%d/", rel, &type, &tag_num, &sent_num))
	return FALSE;
    if (tag_num == -1) return FALSE;

    /* 共参照関係の読み込み */
    if (co_flag && 
	(!strcmp(rel, "=") || !strcmp(rel, "=構") || !strcmp(rel, "=役"))) {

	/* 複数の共参照情報が付与されている場合 */
	if (mention_mgr->mention->entity) {
	    if (mention_mgr->mention->entity->output_num >
		substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention->entity->output_num) {
		mention_mgr->mention->entity->output_num = 
		    substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention->entity->output_num;
	    }
	    else {
		substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention->entity->output_num =
		    mention_mgr->mention->entity->output_num;
	    }	
	}

	mention_ptr = mention_mgr->mention;
	mention_ptr->entity = 
	    substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention->entity;
	mention_ptr->explicit_mention = NULL;

	mention_ptr->salience_score = mention_ptr->entity->salience_score;
	mention_ptr->entity->salience_score += 
	    ((check_feature(tag_ptr->f, "ハ") || check_feature(tag_ptr->f, "モ")) &&
	     check_feature(tag_ptr->f, "係:未格") && !check_feature(tag_ptr->f, "括弧終") ||
	     check_feature(tag_ptr->f, "同格") ||
	     check_feature(tag_ptr->f, "文末")) ? SALIENCE_THEMA : 
	    (check_feature(tag_ptr->f, "読点") && tag_ptr->para_type != PARA_NORMAL ||
	     check_feature(tag_ptr->f, "係:ガ格") ||
	     check_feature(tag_ptr->f, "係:ヲ格")) ? SALIENCE_CANDIDATE : SALIENCE_NORMAL;
	strcpy(mention_ptr->cpp_string, "＊");

	parent_ptr = tag_ptr->parent;
	while (parent_ptr && parent_ptr->para_top_p) parent_ptr = parent_ptr->parent;

	if ((cp = check_feature(tag_ptr->f, "係"))) {
	    strcpy(mention_ptr->spp_string, cp + strlen("係:"));
	} 
	else if (check_feature(tag_ptr->f, "文末")) {
	    strcpy(mention_ptr->spp_string, "文末");
	} 
	else {
	    strcpy(mention_ptr->spp_string, "＊");
	}
	mention_ptr->type = '=';

	/* entityのnameが"の"ならばnameを上書き */
	if (!strcmp(mention_ptr->entity->name, "の") ||
	    mention_ptr->salience_score == 0 && mention_ptr->entity->salience_score > 0) {
	    if (cp = check_feature(tag_ptr->f, "NE")) {
		strcpy(mention_ptr->entity->name, cp + strlen("NE:"));
	    }
	    else if (cp = check_feature(tag_ptr->f, "照応詞候補")) {
		strcpy(mention_ptr->entity->name, cp + strlen("照応詞候補:"));
	    }
	    else {
		strcpy(mention_ptr->entity->name, tag_ptr->head_ptr->Goi2);
	    }
	}
	/* entityのnameがNEでなく、tag_ptrがNEならばnameを上書き */
	if (!strchr(mention_ptr->entity->name, ':') &&
	    (cp = check_feature(tag_ptr->f, "NE"))) {
	    strcpy(mention_ptr->entity->name, cp + strlen("NE:"));
	}
	/* entityのnameがNEでなく、tag_ptrが同格ならばnameを上書き */
	else if (!strchr(mention_ptr->entity->name, ':') &&
		 check_feature(tag_ptr->f, "同格")) {
	    if (cp = check_feature(tag_ptr->f, "照応詞候補")) {
		strcpy(mention_ptr->entity->name, cp + strlen("照応詞候補:"));
	    }
	    else {
		strcpy(mention_ptr->entity->name, tag_ptr->head_ptr->Goi2);
	    }
	}
    }

    /* 共参照以外の関係 */
    else if (!co_flag && 
	     (type == 'N' || type == 'C' || type == 'O' || type == 'D') &&	     
	     /* 用言の場合は省略対象格のみ読み込む */
	     (check_analyze_tag(tag_ptr) == CF_PRED && match_ellipsis_case(rel, NULL) ||
	      /* 名詞の場合は */
	      check_analyze_tag(tag_ptr) == CF_NOUN && 
	      /* 連想照応対象格の場合はそのまま読み込み */
	      (match_ellipsis_case(rel, NULL) ||
	       /* 用言の省略対象格の場合はノ？格として読み込む */
	       match_ellipsis_case(rel, ELLIPSIS_CASE_LIST_VERB) && strcpy(rel, "ノ？"))) &&
	     /* 先行詞は体言のみ */
	     (check_feature(((sp - sent_num)->tag_data + tag_num)->f, "体言") ||
	      check_feature(((sp - sent_num)->tag_data + tag_num)->f, "形副名詞"))) {	

	if (mention_mgr->num >= MENTION_MAX - 1) return FALSE;
	mention_ptr = mention_mgr->mention + mention_mgr->num;
 	mention_ptr->entity = substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention->entity;
	mention_ptr->explicit_mention = (type == 'C') ? substance_tag_ptr((sp - sent_num)->tag_data + tag_num)->mention_mgr.mention : NULL;
	mention_ptr->salience_score = mention_ptr->entity->salience_score;

	mention_ptr->tag_num = mention_mgr->mention->tag_num;
	mention_ptr->sent_num = mention_mgr->mention->sent_num;
	mention_ptr->tag_ptr = (sentence_data + mention_ptr->sent_num - 1)->tag_data + mention_ptr->tag_num;
	mention_ptr->type = type;
	strcpy(mention_ptr->cpp_string, rel);
	if (type == 'C' && (cp = check_feature(((sp - sent_num)->tag_data + tag_num)->f, "係"))) {
	    strcpy(mention_ptr->spp_string, cp + strlen("係:"));
	} 
	else if (type == 'C' && (check_feature(((sp - sent_num)->tag_data + tag_num)->f, "文末"))) {
	    strcpy(mention_ptr->spp_string, "文末");
	} 		
	else {
	    strcpy(mention_ptr->spp_string, "＊");
	}
	mention_mgr->num++;

	/* 共参照タグを辿ると連体修飾先である場合はtypeを'C'に変更 */
	if (type == 'O' && check_feature(tag_ptr->f, "連体修飾") &&
	    tag_ptr->parent->mention_mgr.mention->entity == mention_ptr->entity) {
	    mention_ptr->type = type = 'C';
	}	    

	if (type == 'O') {
	    if (check_analyze_tag(tag_ptr) == CF_PRED)		
		mention_ptr->entity->salience_mem += SALIENCE_ZERO;
	    else 
		mention_ptr->entity->salience_mem += SALIENCE_ASSO;
	}  
    }

    if (!mention_ptr) return FALSE;
    mention_ptr->entity->mention[mention_ptr->entity->mentioned_num] = mention_ptr;
    if (mention_ptr->entity->mentioned_num >= MENTIONED_MAX - 1) { 
	fprintf(stderr, "Entity \"%s\" mentiond too many times!\n", mention_ptr->entity->name);
    }
    else {
	mention_ptr->entity->mentioned_num++;
    }

    /* 学習用情報の出力 */
    if ((OptAnaphora & OPT_TRAIN) && type == 'O' && strcmp(rel, "=")) {

	/* 位置カテゴリの出力 */
	mark_loc_category(sp, tag_ptr);
	mark_nearest_case(sp, tag_ptr);
	entity_ptr = mention_ptr->entity;

	/* 何文以内にmentionを持っているかどうかのチェック */
	diff_sen = 4;
	for (i = 0; i < entity_ptr->mentioned_num; i++) {
	    if (mention_ptr->sent_num == entity_ptr->mention[i]->sent_num &&
		loc_category[(entity_ptr->mention[i]->tag_ptr)->b_ptr->num] == LOC_SELF) continue;	    
	    if (mention_ptr->sent_num - entity_ptr->mention[i]->sent_num < diff_sen)
		diff_sen = mention_ptr->sent_num - entity_ptr->mention[i]->sent_num;
	}

	for (i = 0; i < entity_ptr->mentioned_num; i++) {
	    /* もっとも近くの文に出現したmentionのみ出力 */
	    if (mention_ptr->sent_num - entity_ptr->mention[i]->sent_num > diff_sen) continue;    
	    if ( /* 自分自身はのぞく */
		entity_ptr->mention[i]->sent_num == mention_ptr->sent_num &&
		loc_category[(entity_ptr->mention[i]->tag_ptr)->b_ptr->num] == LOC_SELF) continue;		
	    if (get_location(loc_name, mention_ptr->sent_num, rel, entity_ptr->mention[i], FALSE)) {
		fprintf(Outfp, ";;LOCATION-ANT: %s\n", loc_name);
	    }
	}
    }
    return TRUE;
}

/*==================================================================*/
       void expand_result_to_parallel_entity(TAG_DATA *tag_ptr)
/*==================================================================*/
{
    /* 並列要素を展開する */
    int i, j, result_num;
    CF_TAG_MGR *ctm_ptr = tag_ptr->ctm_ptr; 
    TAG_DATA *t_ptr, *para_ptr;
    ENTITY *entity_ptr, *epnd_entity_ptr;
    MENTION *mention_ptr;
    
    result_num = ctm_ptr->result_num;
    for (i = 0; i < result_num; i++) {
	entity_ptr = entity_manager.entity + ctm_ptr->entity_num[i];

	/* 格要素のentityの省略以外の直近の出現を探す */
	for (j = entity_ptr->mentioned_num - 1; j >= 0; j--) {
	    if (entity_ptr->mention[j]->type == 'S' || entity_ptr->mention[j]->type == '=') break;
	}
	/* 同一文の場合のみを対象とする */
	if (tag_ptr->mention_mgr.mention->sent_num != entity_ptr->mention[j]->sent_num) continue;

	t_ptr = entity_ptr->mention[j]->tag_ptr;

	/* 並列の要素をチェック */
	if (t_ptr->para_type == PARA_NORMAL &&
	    t_ptr->parent && t_ptr->parent->para_top_p) {
	    
	    for (j = 0; t_ptr->parent->child[j]; j++) {
		para_ptr = substance_tag_ptr(t_ptr->parent->child[j]);

		if (para_ptr != t_ptr && check_feature(para_ptr->f, "体言") &&
		    para_ptr->para_type == PARA_NORMAL &&
		    /* 連想照応解析には適用しない(暫定的) */
		    !(ctm_ptr->type[i] == 'O' && check_analyze_tag(tag_ptr) == CF_NOUN) &&
		    /* 省略の場合は拡張先が解析対象の係り先である場合、拡張しない*/
		    !(ctm_ptr->type[i] == 'O' && tag_ptr->parent == para_ptr)) {    

		    epnd_entity_ptr = para_ptr->mention_mgr.mention->entity;
		    ctm_ptr->filled_entity[epnd_entity_ptr->num] = TRUE;
		    ctm_ptr->entity_num[ctm_ptr->result_num] = epnd_entity_ptr->num;
		    ctm_ptr->type[ctm_ptr->result_num] = ctm_ptr->type[i];
		    ctm_ptr->cf_element_num[ctm_ptr->result_num] = ctm_ptr->cf_element_num[i];
		    ctm_ptr->result_num++;

		    if (OptDisplay == OPT_DEBUG)
			fprintf(Outfp, ";;EXPANDED %s : %s -> %s\n", tag_ptr->head_ptr->Goi2, entity_ptr->name, epnd_entity_ptr->name);
		    if (ctm_ptr->result_num == CF_ELEMENT_MAX) return;
		}
	    }
	}
    }
}

/*==================================================================*/
	  void anaphora_result_to_entity(TAG_DATA *tag_ptr)
/*==================================================================*/
{
    /* 照応解析結果ENTITYに関連付ける */
    int i, j;
    char *cp;
    MENTION_MGR *mention_mgr = &(tag_ptr->mention_mgr);
    MENTION *mention_ptr = NULL;
    CF_TAG_MGR *ctm_ptr = tag_ptr->ctm_ptr; 

    /* 格・省略解析結果がない場合は終了 */
    if (!ctm_ptr) return;
    
    for (i = 0; i < ctm_ptr->result_num; i++) {
	if (mention_mgr->num >= MENTION_MAX - 1) return;
	mention_ptr = mention_mgr->mention + mention_mgr->num;
	mention_ptr->entity = entity_manager.entity + ctm_ptr->entity_num[i];
	mention_ptr->tag_num = mention_mgr->mention->tag_num;
	mention_ptr->sent_num = mention_mgr->mention->sent_num;
	mention_ptr->type = ctm_ptr->type[i];
	mention_ptr->tag_ptr = (sentence_data + mention_ptr->sent_num - 1)->tag_data + mention_ptr->tag_num;
	strcpy(mention_ptr->cpp_string, pp_code_to_kstr(ctm_ptr->cf_ptr->pp[ctm_ptr->cf_element_num[i]][0]));
	mention_ptr->salience_score = mention_ptr->entity->salience_score;
	/* 入力側の表層格(格解析結果のみ) */
	if (i < ctm_ptr->case_result_num) {
	    mention_ptr->explicit_mention = ctm_ptr->elem_b_ptr[i]->mention_mgr.mention;
	    if (tag_ptr->tcf_ptr->cf.pp[ctm_ptr->tcf_element_num[i]][0] >= FUKUGOJI_START &&
		tag_ptr->tcf_ptr->cf.pp[ctm_ptr->tcf_element_num[i]][0] <= FUKUGOJI_END) {
		strcpy(mention_ptr->spp_string, pp_code_to_kstr(tag_ptr->tcf_ptr->cf.pp[ctm_ptr->tcf_element_num[i]][0]));
	    }
	    else { 
		if ((cp = check_feature(ctm_ptr->elem_b_ptr[i]->f, "係"))) {
		    strcpy(mention_ptr->spp_string, cp + strlen("係:"));
		} 
		else if (check_feature(ctm_ptr->elem_b_ptr[i]->f, "文末")) {
		    strcpy(mention_ptr->spp_string, "文末");
		}
		else {
		    strcpy(mention_ptr->spp_string, "＊");
		}
	    }
	}
	else {
	    mention_ptr->explicit_mention = NULL;
	    /* 省略でない場合(expand_result_to_parallel_entityで拡張) */
	    if (ctm_ptr->type[i] != 'O') {
		strcpy(mention_ptr->spp_string, "Ｐ");
	    }
	    else {
		strcpy(mention_ptr->spp_string, "Ｏ");
		if (check_analyze_tag(tag_ptr) == CF_PRED) mention_ptr->entity->salience_score += SALIENCE_ZERO;
		else mention_ptr->entity->salience_score += SALIENCE_ASSO;
	    }
	}
	mention_mgr->num++;

	mention_ptr->entity->mention[mention_ptr->entity->mentioned_num] = mention_ptr;
	if (mention_ptr->entity->mentioned_num >= MENTIONED_MAX - 1) { 
	    fprintf(stderr, "Entity \"%s\" mentiond too many times!\n", mention_ptr->entity->name);
	}
	else {
	    mention_ptr->entity->mentioned_num++;
	}
    }  
}

/*==================================================================*/
     int set_tag_case_frame(SENTENCE_DATA *sp, TAG_DATA *tag_ptr, CF_PRED_MGR *cpm_ptr)
/*==================================================================*/
{
    /* ENTITY_PRED_MGRを作成する関数
       make_data_cframeを用いて入力文の格構造を作成するため
       CF_PRED_MGRを作り、そのcfをコピーしている */
    int i;
    TAG_CASE_FRAME *tcf_ptr = tag_ptr->tcf_ptr;
    char *vtype = NULL;  

    /* 入力文側の格要素設定 */
    /* set_data_cf_type(cpm_ptr); */
    if (check_analyze_tag(tag_ptr) == CF_PRED) {
	vtype = check_feature(tag_ptr->f, "用言");
	vtype += strlen("用言:");
	strcpy(cpm_ptr->cf.pred_type, vtype);
	cpm_ptr->cf.type = CF_PRED;
    }
    else {
	strcpy(cpm_ptr->cf.pred_type, "名");
	cpm_ptr->cf.type = CF_NOUN;
    }
    cpm_ptr->cf.type_flag = 0;
    cpm_ptr->cf.voice = tag_ptr->voice;

    /* 入力文の格構造を作成 */
    make_data_cframe(sp, cpm_ptr);
    
    /* ENTITY_PRED_MGRを作成・入力文側の格要素をコピー */
    tcf_ptr->cf = cpm_ptr->cf;
    tcf_ptr->pred_b_ptr = tag_ptr;
    for (i = 0; i < cpm_ptr->cf.element_num; i++) {
	tcf_ptr->elem_b_ptr[i] = substance_tag_ptr(cpm_ptr->elem_b_ptr[i]);
	tcf_ptr->elem_b_num[i] = cpm_ptr->elem_b_num[i];
    }

    return TRUE;
}

/*==================================================================*/
int set_cf_candidate(TAG_DATA *tag_ptr, CASE_FRAME **cf_array, int check_result)
/*==================================================================*/
{
    int i, frame_num = 0;

    if (check_result == CF_PRED) {	
	for (i = 0; i < tag_ptr->cf_num; i++) {
	    if ((tag_ptr->cf_ptr + i)->type == tag_ptr->tcf_ptr->cf.type) {
		*(cf_array + frame_num++) = tag_ptr->cf_ptr + i;
	    }
	}
    }
    else {
	for (i = 0; i < tag_ptr->ncf_num; i++) {
	    if ((tag_ptr->ncf_ptr + i)->type == tag_ptr->tcf_ptr->cf.type) {
		*(cf_array + frame_num++) = tag_ptr->ncf_ptr + i;
	    }
	}
    }
    return frame_num;
}

/*==================================================================*/
double calc_score_of_ctm(CF_TAG_MGR *ctm_ptr, TAG_CASE_FRAME *tcf_ptr)
/*==================================================================*/
{
    /* 格フレームとの対応付けのスコアを計算する関数  */
    int i, j, e_num, debug = 0;
    double score;
    char key[SMALL_DATA_LEN];

    /* 対象の格フレームが選択されることのスコア */
    score = get_cf_probability_for_pred(&(tcf_ptr->cf), ctm_ptr->cf_ptr);

    /* 対応付けられた要素に関するスコア(格解析結果) */
    for (i = 0; i < ctm_ptr->case_result_num; i++) {
	e_num = ctm_ptr->cf_element_num[i];
	
	score += get_ex_probability_with_para(ctm_ptr->tcf_element_num[i], &(tcf_ptr->cf), e_num, ctm_ptr->cf_ptr) +
	    get_case_function_probability_for_pred(ctm_ptr->tcf_element_num[i], &(tcf_ptr->cf), e_num, ctm_ptr->cf_ptr, TRUE);
	
	if (OptDisplay == OPT_DEBUG && debug)
	    fprintf(Outfp, ";;対応あり:%s-%s:%f:%f ", ctm_ptr->elem_b_ptr[i]->head_ptr->Goi2, 
		   pp_code_to_kstr(ctm_ptr->cf_ptr->pp[e_num][0]),
		   get_ex_probability_with_para(ctm_ptr->tcf_element_num[i], &(tcf_ptr->cf), e_num, ctm_ptr->cf_ptr),
		   get_case_function_probability_for_pred(ctm_ptr->tcf_element_num[i], &(tcf_ptr->cf), e_num, ctm_ptr->cf_ptr, TRUE));
    }

    /* 入力文の格要素のうち対応付けられなかった要素に関するスコア */
    for (i = 0; i < tcf_ptr->cf.element_num - ctm_ptr->case_result_num; i++) {
	if (OptDisplay == OPT_DEBUG && debug) 
	    fprintf(Outfp, ";;対応なし:%s:%f ", (tcf_ptr->elem_b_ptr[ctm_ptr->non_match_element[i]])->head_ptr->Goi2, score);	
	score += FREQ0_ASSINED_SCORE + UNKNOWN_CASE_SCORE;
    }
    if (OptDisplay == OPT_DEBUG && debug) fprintf(Outfp, ";; %f ", score);	   

    /* 格フレームの格が埋まっているかどうかに関するスコア */
    for (e_num = 0; e_num < ctm_ptr->cf_ptr->element_num; e_num++) {
	if (tcf_ptr->cf.type == CF_NOUN) continue;
	score += get_case_probability(e_num, ctm_ptr->cf_ptr, ctm_ptr->filled_element[e_num], NULL);	
    }
    if (OptDisplay == OPT_DEBUG && debug) fprintf(Outfp, ";; %f\n", score);

    return score;
}

/*==================================================================*/
		int convert_locname_id(char *loc_name)
/*==================================================================*/
{
    /* 位置カテゴリをIDに変換[0-83]、その他:-1 */
    /* 出現格: S＊ + [CO][ガヲニ]: 7  */
    /* 位置カ: C[1-9] + B[1-3]	 : 12 */
    int id = 0;

    /* ヲ-Oヲ-C8, ニ-Oガ-B1 */
    if (strlen(loc_name) != BYTES4CHAR * 2 + 5) return -1;

    /* [SCO] */
    if (loc_name[BYTES4CHAR + 1] == 'C') id += 12;
    else if (loc_name[BYTES4CHAR + 1] == 'O') id += 48;
    else if (loc_name[BYTES4CHAR + 1] != 'S') return -1;
    
    /* [ガヲニ] */
    if (loc_name[BYTES4CHAR + 1] != 'S') {
	if (!strncmp(loc_name + BYTES4CHAR + 2, "ヲ", BYTES4CHAR)) id += 12;
	else if (!strncmp(loc_name + BYTES4CHAR + 2, "ニ", BYTES4CHAR)) id += 24;
	else if (strncmp(loc_name + BYTES4CHAR + 2, "ガ", BYTES4CHAR)) return -1;
    }

    /* [CB] */
    if (loc_name[BYTES4CHAR * 2 + 3] == 'B') id += 9;
    else if (loc_name[BYTES4CHAR * 2 + 3] != 'C') return -1;

    /* [1-9] */
    if (atoi(loc_name + BYTES4CHAR * 2 + 4) > 0) id += atoi(loc_name + BYTES4CHAR * 2 + 4) - 1;
    else return -1;

    return id;
}

/*==================================================================*/
double calc_ellipsis_score_of_ctm(CF_TAG_MGR *ctm_ptr, TAG_CASE_FRAME *tcf_ptr)
/*==================================================================*/
{
    /* 格フレームとの対応付けのスコアを計算する関数(省略解析の評価) */
    int i, j, loc_num, e_num, sent_num, pp;
    double score = 0, max_score, tmp_ne_ct_score, tmp_score, ex_prob, prob, penalty;
    double *of_ptr, scase_prob_cs, scase_prob, location_prob;
    char *cp, key[SMALL_DATA_LEN], loc_name[SMALL_DATA_LEN];
    ENTITY *entity_ptr;

    /* 解析対象の基本句の文番号 */
    sent_num = tcf_ptr->pred_b_ptr->mention_mgr.mention->sent_num;

    /* omit_featureの初期化 */
    for (i = 0; i < ELLIPSIS_CASE_NUM; i++) {
	for (j = 0; j < O_FEATURE_NUM; j++) {
	    ctm_ptr->omit_feature[i][j] = INITIAL_SCORE;
	}
    }

    /* 対応付けられた要素に関するスコア(省略解析結果) */
    for (i = ctm_ptr->case_result_num; i < ctm_ptr->result_num; i++) {
	e_num = ctm_ptr->cf_element_num[i];
	entity_ptr = entity_manager.entity + ctm_ptr->entity_num[i]; /* 関連付けられたENTITY */	
	pp = (tcf_ptr->cf.type == CF_NOUN) ? 1 /* 連想照応解析の場合は1とする(暫定的) */
	    : ctm_ptr->cf_ptr->pp[e_num][0];   /* "が"、"を"、"に"のcodeはそれぞれ1、2、3 */
	of_ptr = ctm_ptr->omit_feature[pp - 1];

	/* 埋まったかどうか */
	of_ptr[ASSIGNED] = 1;

	/* 対応付けられた解析対象格の埋まりやすさ */
	of_ptr[NO_ASSIGNMENT] = get_case_probability(e_num, ctm_ptr->cf_ptr, TRUE, NULL);

	/* P(弁当|食べる:動2,ヲ格)/P(弁当) (∝P(食べる:動2,ヲ格|弁当)) */
	/* type='S'または'='のmentionの中で最大となるものを使用 */	
	max_score = INITIAL_SCORE;

	for (j = 0; j < entity_ptr->mentioned_num; j++) {
	    if (entity_ptr->mention[j]->type != 'S' && entity_ptr->mention[j]->type != '=') continue;
	    tmp_ne_ct_score = FREQ0_ASSINED_SCORE;

 	    /* カテゴリがある場合はP(食べる:動2,ヲ格|カテゴリ:人)もチェック */
	    if ((OptGeneralCF & OPT_CF_CATEGORY) && 
		(cp = check_feature(entity_ptr->mention[j]->tag_ptr->head_ptr->f, "カテゴリ"))) {

		while (strchr(cp, ':') && (cp = strchr(cp, ':')) || (cp = strchr(cp, ';'))) {
		    sprintf(key, "CT:%s:", ++cp);
		    if (strchr(key + 3, ';')) *strchr(key + 3, ';') = ':'; /* tag = CT:組織・団体;抽象物: */
		    
		    if ((prob = get_ex_ne_probability(key, e_num, ctm_ptr->cf_ptr, TRUE))) {
			/* P(カテゴリ:人|食べる:動2,ヲ格) */
			tmp_score = log(prob);

			/* /P(カテゴリ:人) */
			*strchr(key + 3, ':') = '\0';
			tmp_score -= get_general_probability(key, "KEY");		
			if (tmp_score > of_ptr[CEX_PMI]) of_ptr[CEX_PMI] = tmp_score;
			if (tmp_score > tmp_ne_ct_score) tmp_ne_ct_score = tmp_score;
		    }
		}
	    }

	    /* 固有表現の場合はP(食べる:動2,ヲ格|ARTIFACT)もチェック */
	    if ((OptGeneralCF & OPT_CF_NE) && 
		(cp = check_feature(entity_ptr->mention[j]->tag_ptr->f, "NE")) &&
		strlen(cp) < SMALL_DATA_LEN && /* SMALL_DATA_LENより長い素性は読み込めないので無視 */
		(prob = get_ex_ne_probability(cp, e_num, ctm_ptr->cf_ptr, TRUE))) {

		/* P(ARTIFACT|食べる:動2,ヲ格) */
		tmp_score = log(prob);

		/* /P(ARTIFACT) */
		strcpy(key, cp);
		*strchr(key + 3, ':') = '\0'; /* key = NE:LOCATION */
		tmp_score -= get_general_probability(key, "KEY");		
		if (tmp_score > of_ptr[NEX_PMI]) of_ptr[NEX_PMI] = tmp_score;
		if (tmp_score > tmp_ne_ct_score) tmp_ne_ct_score = tmp_score;
	    }

	    /* P(弁当|食べる:動2,ヲ格) */
	    tmp_score = ex_prob = get_ex_probability(ctm_ptr->tcf_element_num[i], &(tcf_ptr->cf), 
						     entity_ptr->mention[j]->tag_ptr, e_num, ctm_ptr->cf_ptr, FALSE);

	    /* /P(弁当) */
	    tmp_score -= get_key_probability(entity_ptr->mention[j]->tag_ptr);	    
	    if (tmp_score > of_ptr[EX_PMI]) of_ptr[EX_PMI] = tmp_score;
	    
	    /* カテゴリ、固有表現から計算された値との平均値を使用 */
	    if (ex_prob > FREQ0_ASSINED_SCORE && tmp_ne_ct_score > FREQ0_ASSINED_SCORE)	tmp_score = (tmp_score + tmp_ne_ct_score) / 2;
	    else if (tmp_ne_ct_score > FREQ0_ASSINED_SCORE) tmp_score = tmp_ne_ct_score;	

	    if (tmp_score > max_score) {
		max_score = tmp_score;
	    }
	}
	score += max_score;

	/* SALIENCE_SCORE */
	of_ptr[SALIENCE_CHECK] = (entity_ptr->salience_score >= SALIENCE_THRESHOLDF) ? 1 : 0;

	/* mentionごとにスコアを計算 */	
	max_score = FREQ0_ASSINED_SCORE;
	for (j = 0; j < entity_ptr->mentioned_num; j++) {
	    tmp_score = 0;

	    /* 自分自身は除外 */
	    if (entity_ptr->mention[j]->sent_num == sent_num &&
		!loc_category[(entity_ptr->mention[j]->tag_ptr)->b_ptr->num]) continue;

	    /* 位置カテゴリであまり考慮できない情報を追加 */
	    /* NE_PERSON */
	    if (check_feature(entity_ptr->mention[j]->tag_ptr->f, "NE:PERSON") ||
		check_feature(entity_ptr->mention[j]->tag_ptr->head_ptr->f, "人名末尾") &&
		check_feature(entity_ptr->mention[j]->tag_ptr->b_ptr->tag_ptr->f, "NE:PERSON")) {
		of_ptr[NE_PERSON] = 1;
	    }
	    /* CACHE */
	    if (!strcmp(entity_ptr->mention[j]->tag_ptr->head_ptr->Goi, tcf_ptr->pred_b_ptr->head_ptr->Goi) &&
		entity_ptr->mention[j]->type == 'C' && !strcmp(pp_code_to_kstr(pp), entity_ptr->mention[j]->cpp_string)) {
		of_ptr[CACHE] = 1;
	    }
	    /* NEAREST_CASE */
	    if (entity_ptr->mention[j]->type == 'S' || entity_ptr->mention[j]->type == '=') {
		if (entity_ptr->mention[j]->sent_num == nearest_case_sen[0] &&
		    entity_ptr->mention[j]->tag_ptr->num == nearest_case_tag[0]) of_ptr[NEAREST_GA] = 1;
		if (entity_ptr->mention[j]->sent_num == nearest_case_sen[1] &&
		    entity_ptr->mention[j]->tag_ptr->num == nearest_case_tag[1]) of_ptr[NEAREST_WO] = 1;
		if (entity_ptr->mention[j]->sent_num == nearest_case_sen[2] &&
		    entity_ptr->mention[j]->tag_ptr->num == nearest_case_tag[2]) of_ptr[NEAREST_NI] = 1;
		if (entity_ptr->mention[j]->sent_num == nearest_case_sen[3] &&
		    entity_ptr->mention[j]->tag_ptr->num == nearest_case_tag[3]) of_ptr[NEAREST_WA] = 1;
	    }
	    
	    /* 位置カテゴリ */
	    /* 省略格、type(S,=,O,N,C)ごとに位置カテゴリごとに先行詞となる確率を考慮
	       位置カテゴリは、以前の文であれば B + 何文前か(4文前以上は0)
	       同一文内であれば C + loc_category という形式(ex. ガ-O-C3、ヲ-=-B2) */
	    if (tcf_ptr->cf.type == CF_PRED) {
		/* NORMAL */
		get_location(loc_name, sent_num, pp_code_to_kstr(pp), entity_ptr->mention[j], FALSE);		
		location_prob = get_general_probability("PMI", loc_name);
		loc_num = convert_locname_id(loc_name);
		if (loc_num != -1) of_ptr[LOCATION_S + loc_num] = 1;
		
		/* COMPLEMENT */
		if (get_location_comp(loc_name, sent_num, pp_code_to_kstr(pp), entity_ptr->mention[j])) {
		    loc_num = convert_locname_id(loc_name);
		    if (loc_num != -1) of_ptr[LOCATION_S + loc_num] = 1;
		}
	    }
	    else {
		get_location(loc_name, sent_num, pp_code_to_kstr(pp), entity_ptr->mention[j], TRUE);
		location_prob = get_general_probability("T", loc_name);
	    }
	    tmp_score += location_prob;

	    if (tmp_score > max_score) {
		max_score = tmp_score;
		/* 最大のスコアとなった基本句を保存(並列への対処のため) */
		ctm_ptr->elem_b_ptr[i] = entity_ptr->mention[j]->tag_ptr;
	    }
	}
	score += max_score;
    }

    /* 対応付けられなかった解析対象格の埋まりにくさ */
    for (e_num = 0; e_num < ctm_ptr->cf_ptr->element_num; e_num++) {
        if (!ctm_ptr->filled_element[e_num] &&
	    match_ellipsis_case(pp_code_to_kstr(ctm_ptr->cf_ptr->pp[e_num][0]), NULL) &&
	    ctm_ptr->cf_ptr->oblig[e_num]) {
	    of_ptr = ctm_ptr->omit_feature[ctm_ptr->cf_ptr->pp[e_num][0] - 1];
	    of_ptr[NO_ASSIGNMENT] = get_case_probability(e_num, ctm_ptr->cf_ptr, FALSE, NULL);
            score += of_ptr[NO_ASSIGNMENT];
	}
    }

    return score;
}

/*==================================================================*/
     int copy_ctm(CF_TAG_MGR *source_ctm, CF_TAG_MGR *target_ctm)
/*==================================================================*/
{
    int i, j;

    target_ctm->score = source_ctm->score;
    target_ctm->cf_ptr = source_ctm->cf_ptr;
    target_ctm->result_num = source_ctm->result_num;
    target_ctm->case_result_num = source_ctm->case_result_num;
    for (i = 0; i < CF_ELEMENT_MAX; i++) {
	target_ctm->filled_element[i] = source_ctm->filled_element[i];
	target_ctm->non_match_element[i] = source_ctm->non_match_element[i];
	target_ctm->cf_element_num[i] = source_ctm->cf_element_num[i];
	target_ctm->tcf_element_num[i] = source_ctm->tcf_element_num[i];
	target_ctm->entity_num[i] = source_ctm->entity_num[i];
	target_ctm->elem_b_ptr[i] = source_ctm->elem_b_ptr[i];
	target_ctm->type[i] = source_ctm->type[i];
    }
    target_ctm->overt_arguments_score = source_ctm->overt_arguments_score;    
    for (i = 0; i < ELLIPSIS_CASE_NUM; i++) {
	for (j = 0; j < O_FEATURE_NUM; j++) {
	    target_ctm->omit_feature[i][j] = source_ctm->omit_feature[i][j];
	}
    }
}

/*==================================================================*/
      int preserve_ctm(CF_TAG_MGR *ctm_ptr, int start, int num)
/*==================================================================*/
{
    /* start番目からnum個のwork_ctmのスコアと比較し上位ならば保存する
       num個のwork_ctmのスコアは降順にソートされていることを仮定している
       保存された場合は1、されなかった場合は0を返す */
    int i, j;
    
    for (i = start; i < start + num; i++) {
	
	/* work_ctmに結果を保存 */
	if (ctm_ptr->score > work_ctm[i].score) {	    
	    for (j = start + num - 1; j > i; j--) {
		if (work_ctm[j - 1].score > INITIAL_SCORE) {
		    copy_ctm(&work_ctm[j - 1], &work_ctm[j]);
		}
	    }
	    copy_ctm(ctm_ptr, &work_ctm[i]);
	    return TRUE;
	}
    }
    return FALSE;
}

/*==================================================================*/
int case_analysis_for_anaphora(TAG_DATA *tag_ptr, CF_TAG_MGR *ctm_ptr, int i, int r_num)
/*==================================================================*/
{
    /* 候補の格フレームについて照応解析用格解析を実行する関数
       再帰的に呼び出す
       iにはtag_ptr->tcf_ptr->cf.element_numのうちチェックした数 
       r_numにはそのうち格フレームと関連付けられた要素の数が入る */   
    int j, k, e_num;

    /* すでに埋まっている格フレームの格をチェック */
    memset(ctm_ptr->filled_element, 0, sizeof(int) * CF_ELEMENT_MAX);
    for (j = 0; j < r_num; j++) {
	ctm_ptr->filled_element[ctm_ptr->cf_element_num[j]] = TRUE;
	
	/* 類似している格も埋まっているものとして扱う */
	for (k = 0; ctm_ptr->cf_ptr->samecase[k][0] != END_M; k++) {
	    if (ctm_ptr->cf_ptr->samecase[k][0] == ctm_ptr->cf_element_num[j])
		ctm_ptr->filled_element[ctm_ptr->cf_ptr->samecase[k][1]] = TRUE;
	    else if (ctm_ptr->cf_ptr->samecase[k][1] == ctm_ptr->cf_element_num[j])
		ctm_ptr->filled_element[ctm_ptr->cf_ptr->samecase[k][0]] = TRUE;
	}
    }
    
    /* まだチェックしていない要素がある場合 */
    if (i < tag_ptr->tcf_ptr->cf.element_num) {

	/* 入力文のi番目の格要素の取りうる格(cf.pp[i][j])を順番にチェック */
	for (j = 0; tag_ptr->tcf_ptr->cf.pp[i][j] != END_M; j++) {

	    /* 入力文のi番目の格要素を格フレームのcf.pp[i][j]格に割り当てる */
	    for (e_num = 0; e_num < ctm_ptr->cf_ptr->element_num; e_num++) {

		if (tag_ptr->tcf_ptr->cf.pp[i][j] == ctm_ptr->cf_ptr->pp[e_num][0] &&
		    (tag_ptr->tcf_ptr->cf.type != CF_NOUN || 
		     check_feature(tag_ptr->tcf_ptr->elem_b_ptr[i]->f, "係:ノ格"))) {
		    
		    /* 対象の格が既に埋まっている場合は不可 */
		    if (ctm_ptr->filled_element[e_num] == TRUE) continue;

		    /* 非格要素は除外 */
		    if (check_feature(tag_ptr->tcf_ptr->elem_b_ptr[i]->f, "非格要素")) {
			continue;
		    }	
	    		    
		    /* 名詞格フレームの格は"φ"となっているので表示用"ノ"に変更 */
		    if (tag_ptr->tcf_ptr->cf.type == CF_NOUN) {
			ctm_ptr->cf_ptr->pp[e_num][0] = pp_kstr_to_code("ノ");
		    }

		    /* 対応付け結果を記録 */
		    ctm_ptr->elem_b_ptr[r_num] = tag_ptr->tcf_ptr->elem_b_ptr[i];
		    ctm_ptr->cf_element_num[r_num] = e_num;
		    ctm_ptr->tcf_element_num[r_num] = i;
    		    ctm_ptr->type[r_num] = tag_ptr->tcf_ptr->elem_b_num[i] == -1 ? 'N' : 'C';
		    ctm_ptr->entity_num[r_num] = ctm_ptr->elem_b_ptr[r_num]->mention_mgr.mention->entity->num;

		    /* i+1番目の要素のチェックへ */
		    case_analysis_for_anaphora(tag_ptr, ctm_ptr, i + 1, r_num + 1);
		}
	    }    
	}

	/* 格要素を割り当てない場合 */
	/* 入力文のi番目の要素が対応付けられなかったことを記録 */
	ctm_ptr->non_match_element[i - r_num] = i; 
	case_analysis_for_anaphora(tag_ptr, ctm_ptr, i + 1, r_num);
    }

    /* すべてのチェックが終了した場合 */
    else {
	/* この段階でr_num個が対応付けられている */
	ctm_ptr->result_num = ctm_ptr->case_result_num = r_num;
	/* スコアを計算 */
	ctm_ptr->score = ctm_ptr->overt_arguments_score = calc_score_of_ctm(ctm_ptr, tag_ptr->tcf_ptr);
	/* スコア上位を保存 */
	preserve_ctm(ctm_ptr, 0, CASE_CANDIDATE_MAX);	
    }

    return TRUE;
}

/*==================================================================*/
int ellipsis_analysis(TAG_DATA *tag_ptr, CF_TAG_MGR *ctm_ptr, int i, int r_num)
/*==================================================================*/
{
    /* 候補となる格フレームと格要素の対応付けについて省略解析を実行する関数
       再帰的に呼び出す 
       iにはELLIPSIS_CASE_LIST[]のうちチェックした数が入る
       r_numには格フレームと関連付けられた要素の数が入る
       (格解析の結果関連付けられたものも含む) */
    int j, k, e_num;
    TAG_DATA *para_ptr;
    int pre_filled_element[CF_ELEMENT_MAX], pre_filled_entity[ENTITY_MAX];

    /* 再帰前のfilled_element, filled_entityを保存 */
    memcpy(pre_filled_element, ctm_ptr->filled_element, sizeof(int) * CF_ELEMENT_MAX);
    memcpy(pre_filled_entity, ctm_ptr->filled_entity, sizeof(int) * ENTITY_MAX);

    /* すでに埋まっている格フレームの格をチェック */
    memset(ctm_ptr->filled_element, 0, sizeof(int) * CF_ELEMENT_MAX);
    memset(ctm_ptr->filled_entity, 0, sizeof(int) * ENTITY_MAX);
    for (j = 0; j < r_num; j++) {
	/* 埋まっている格をチェック */
	ctm_ptr->filled_element[ctm_ptr->cf_element_num[j]] = TRUE;
	/* 類似している格も埋まっているものとして扱う */
	for (k = 0; ctm_ptr->cf_ptr->samecase[k][0] != END_M; k++) {
	    if (ctm_ptr->cf_ptr->samecase[k][0] == ctm_ptr->cf_element_num[j])
		ctm_ptr->filled_element[ctm_ptr->cf_ptr->samecase[k][1]] = TRUE;
	    else if (ctm_ptr->cf_ptr->samecase[k][1] == ctm_ptr->cf_element_num[j])
		ctm_ptr->filled_element[ctm_ptr->cf_ptr->samecase[k][0]] = TRUE;
	}
	/* 格を埋めた要素をチェック */
	ctm_ptr->filled_entity[ctm_ptr->entity_num[j]] = TRUE;

	/* 並列要素もチェック */
	if (j < ctm_ptr->case_result_num && /* 格解析結果の場合 */
	    check_feature(ctm_ptr->elem_b_ptr[j]->f, "体言") &&
	    substance_tag_ptr(ctm_ptr->elem_b_ptr[j])->para_type == PARA_NORMAL) {
    
	    for (k = 0; substance_tag_ptr(ctm_ptr->elem_b_ptr[j])->parent->child[k]; k++) {
		para_ptr = substance_tag_ptr(substance_tag_ptr(ctm_ptr->elem_b_ptr[j])->parent->child[k]);
		ctm_ptr->filled_entity[para_ptr->mention_mgr.mention->entity->num] = TRUE;
	    }
	}
	else if ( /* 省略解析結果の場合は同一文の場合のみ考慮 */    
	    entity_manager.entity[ctm_ptr->entity_num[j]].mention[0]->sent_num == tag_ptr->mention_mgr.mention->sent_num &&
	    entity_manager.entity[ctm_ptr->entity_num[j]].mention[0]->tag_ptr->para_type == PARA_NORMAL) {
	    
	    for (k = 0; entity_manager.entity[ctm_ptr->entity_num[j]].mention[0]->tag_ptr->parent->child[k]; k++) {
		para_ptr = substance_tag_ptr(entity_manager.entity[ctm_ptr->entity_num[j]].mention[0]->tag_ptr->parent->child[k]);
		ctm_ptr->filled_entity[para_ptr->mention_mgr.mention->entity->num] = TRUE;
	    }
	}
    }

    /* 自分自身も不可 */
    ctm_ptr->filled_entity[tag_ptr->mention_mgr.mention->entity->num] = TRUE;

    /* 自分の係り先は不可 */
    if (tag_ptr->parent &&
	(check_analyze_tag(tag_ptr) == CF_PRED || check_feature(tag_ptr->f, "係:ノ格"))) {
	ctm_ptr->filled_entity[substance_tag_ptr(tag_ptr->parent)->mention_mgr.mention->entity->num] = TRUE;
    }
    /* 係り先の並列要素 */
    if (tag_ptr->parent && check_feature(tag_ptr->parent->f, "体言") &&
	tag_ptr->parent->para_top_p) {
	
	for (j = 0; tag_ptr->parent->child[j]; j++) {
	    para_ptr = substance_tag_ptr(tag_ptr->parent->child[j]);
	    
	    if (para_ptr->num > tag_ptr->num &&
		para_ptr != tag_ptr && check_feature(para_ptr->f, "体言") &&
		para_ptr->para_type == PARA_NORMAL)
		ctm_ptr->filled_entity[para_ptr->mention_mgr.mention->entity->num] = TRUE;
	}
    }
   
    /* 自分に係る要素は格解析で処理済みなので不可 */
    for (j = 0; tag_ptr->child[j]; j++) {
	ctm_ptr->filled_entity[substance_tag_ptr(tag_ptr->child[j])->mention_mgr.mention->entity->num] = TRUE;
    }  

    /* 自分と並列な要素も不可(連想照応解析の場合) */
    if (check_analyze_tag(tag_ptr) == CF_NOUN && tag_ptr->para_type == PARA_NORMAL &&
	tag_ptr->parent && tag_ptr->parent->para_top_p) {
	
	for (j = 0; tag_ptr->parent->child[j]; j++) {
	    para_ptr = substance_tag_ptr(tag_ptr->parent->child[j]);
	    if (para_ptr != tag_ptr && check_feature(para_ptr->f, "体言") &&
		para_ptr->para_type == PARA_NORMAL)
		ctm_ptr->filled_entity[para_ptr->mention_mgr.mention->entity->num] = TRUE;
	}
    }

    /* まだチェックしていない省略解析対象格がある場合 */
    if (*ELLIPSIS_CASE_LIST[i]) {
	/* 格がELLIPSIS_CASE_LIST[i]と一致する格スロットを探す(e_numがelement_numより小さい場合は一致する格スロットあり) */
	for (e_num = 0; e_num < ctm_ptr->cf_ptr->element_num; e_num++) {
	    /* 名詞の場合は格スロットの番号とiを一致させる */
            if (tag_ptr->tcf_ptr->cf.type == CF_NOUN) {
                ctm_ptr->cf_ptr->pp[e_num][0] = pp_kstr_to_code("ノ");
                if (e_num == i && !strcmp("ノ", ELLIPSIS_CASE_LIST[i])) break;
	    }
            /* 用言の場合は格をチェック */
            else if (ctm_ptr->cf_ptr->pp[e_num][0] == pp_kstr_to_code(ELLIPSIS_CASE_LIST[i])) break;
	}

	/* 対象の格が格フレームに存在しない場合は次の格へ */
	if (e_num == ctm_ptr->cf_ptr->element_num) {	    
	    ellipsis_analysis(tag_ptr, ctm_ptr, i + 1, r_num);
	}
	else { /* 対象の格が格フレームに存在する場合 */
	    /* すでに埋まっている場合は次の格をチェックする */
	    if (ctm_ptr->filled_element[e_num] == TRUE) {
		ellipsis_analysis(tag_ptr, ctm_ptr, i + 1, r_num);
	    }
	    else { /* 埋まっていない場合は候補を埋める */
 		for (k = 0; k < entity_manager.num; k++) {
		    /* salience_scoreがSALIENCE_THRESHOLD以下なら候補としない */
		    if ((tag_ptr->tcf_ptr->cf.type == CF_NOUN && entity_manager.entity[k].salience_score <= SALIENCE_THRESHOLDN) ||
			(entity_manager.entity[k].salience_score <= SALIENCE_THRESHOLD)) continue;
		    
		    /* 対象のENTITYがすでに対応付けられている場合は不可 */
		    if (ctm_ptr->filled_entity[k]) continue;

		    /* 疑問詞は先行詞候補から除外(暫定的) */
		    if (check_feature(entity_manager.entity[k].mention[0]->tag_ptr->f, "疑問詞")) continue;

		    /* 対応付け結果を記録(基本句との対応付けは取っていないためelem_b_ptrは使用しない) */
		    ctm_ptr->cf_element_num[r_num] = e_num;
		    ctm_ptr->entity_num[r_num] = k;
		    
		    /* 候補を埋めて次の格のチェックへ */
		    ellipsis_analysis(tag_ptr, ctm_ptr, i + 1, r_num + 1);
		}
		/* 埋めないで次の格へ(不特定) */
                ellipsis_analysis(tag_ptr, ctm_ptr, i + 1, r_num);
 	    }
	}
    }
    
    /* すべてのチェックが終了した場合 */
    else {
	/* この段階でr_num個が対応付けられている */
	ctm_ptr->result_num = r_num;
	for (j = ctm_ptr->case_result_num; j < r_num; j++) ctm_ptr->type[j] = 'O';

	/* スコアを計算(旧モデル、連想照応解析に使用) */
	if (tag_ptr->tcf_ptr->cf.type == CF_NOUN) {
	    ctm_ptr->score = calc_ellipsis_score_of_ctm(ctm_ptr, tag_ptr->tcf_ptr) + ctm_ptr->overt_arguments_score;
	}
	/* スコアを計算(線形対数モデル) */
	else {
	    calc_ellipsis_score_of_ctm(ctm_ptr, tag_ptr->tcf_ptr);
	    ctm_ptr->score = ctm_ptr->overt_arguments_score * overt_arguments_weight;
    	    for (j = 0; j < ELLIPSIS_CASE_NUM; j++) {
		for (k = 0; k < O_FEATURE_NUM; k++) {
		    ctm_ptr->score += (ctm_ptr->omit_feature[j][k] == INITIAL_SCORE) ?
			0 : ctm_ptr->omit_feature[j][k] * case_feature_weight[j][k];
		}
	    }   
	}

	/* スコア上位を保存 */
	preserve_ctm(ctm_ptr, CASE_CANDIDATE_MAX, ELLIPSIS_RESULT_MAX);
    }   
    
    /* filled_element, filled_entityを元に戻す */
    memcpy(ctm_ptr->filled_element, pre_filled_element, sizeof(int) * CF_ELEMENT_MAX);
    memcpy(ctm_ptr->filled_entity, pre_filled_entity, sizeof(int) * ENTITY_MAX);
    return TRUE;
}

/*==================================================================*/
   int ellipsis_analysis_main(TAG_DATA *tag_ptr, int check_result)
/*==================================================================*/
{
    /* ある基本句を対象として省略解析を行う関数 */
    /* 格フレームごとにループを回す */
    int i, j, k, frame_num = 0, rnum_check_flag, aresult[PP_NUMBER], gresult[PP_NUMBER], true_flag;
    char *cp, tmp_string[WORD_LEN_MAX], aresult_s[WORD_LEN_MAX], gresult_s[WORD_LEN_MAX];
    CASE_FRAME **cf_array;
    CF_TAG_MGR *ctm_ptr = work_ctm + CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX;
    MENTION *mention_ptr;

    /* 使用する格フレームの設定 */
    if (check_result == CF_PRED) {
	cf_array = (CASE_FRAME **)malloc_data(sizeof(CASE_FRAME *)*tag_ptr->cf_num, "ellipsis_analysis_main");
    }
    else {
	cf_array = (CASE_FRAME **)malloc_data(sizeof(CASE_FRAME *)*tag_ptr->ncf_num, "ellipsis_analysis_main");
    }
    frame_num = set_cf_candidate(tag_ptr, cf_array, check_result);
    
    if (OptDisplay == OPT_DEBUG) fprintf(Outfp, ";;CASE FRAME NUM: %d\n", frame_num);

    /* work_ctmのスコアを初期化 */
    for (i = 0; i < CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX; i++) 
	work_ctm[i].score = INITIAL_SCORE;

    /* FRAME_FOR_ZERO_MAX個以上の格フレームはチェックしない */
    if (frame_num > FRAME_FOR_ZERO_MAX) frame_num = FRAME_FOR_ZERO_MAX;

    /* 照応解析用格解析(上位CASE_CANDIDATE_MAX個の結果を保持する) */
    for (i = 0; i < frame_num; i++) {

	/* OR の格フレーム(和フレーム)を除く */
	if (((*(cf_array + i))->etcflag & CF_SUM) && frame_num != 1) {
	    continue;
	}

	/* ctm_ptrの初期化 */
	ctm_ptr->score = INITIAL_SCORE;

	/* 格フレームを指定 */
 	ctm_ptr->cf_ptr = *(cf_array + i);

	/* 格解析 */
	case_analysis_for_anaphora(tag_ptr, ctm_ptr, 0, 0);	
    }
    if (work_ctm[0].score == INITIAL_SCORE) return FALSE;

    if (OptDisplay == OPT_DEBUG || OptAnaphora & OPT_PRINT_ENTITY) {
	for (i = 0; i < CASE_CANDIDATE_MAX; i++) {
	    if (work_ctm[i].score == INITIAL_SCORE) break;
	    fprintf(Outfp, ";;格解析候補%d-%d:%2d %.3f %s",
		   tag_ptr->mention_mgr.mention->sent_num, tag_ptr->num,
		   i + 1, work_ctm[i].score, work_ctm[i].cf_ptr->cf_id);
	    for (j = 0; j < work_ctm[i].result_num; j++) {
		fprintf(Outfp, " %s%s:%s",
		       work_ctm[i].cf_ptr->adjacent[work_ctm[i].cf_element_num[j]] ? "*" : "-",
		       pp_code_to_kstr(work_ctm[i].cf_ptr->pp[work_ctm[i].cf_element_num[j]][0]),
		       work_ctm[i].elem_b_ptr[j]->head_ptr->Goi2);
	    }
	    for (j = 0; j < work_ctm[i].cf_ptr->element_num; j++) {
		if (!work_ctm[i].filled_element[j] && 
		    match_ellipsis_case(pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]), NULL))
		    fprintf(Outfp, " %s:×", pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]));
	    }
	    fprintf(Outfp, "\n");
	}
    }
    
    /* 上記の対応付けに対して省略解析を実行する */
    for (i = 0; i < CASE_CANDIDATE_MAX; i++) {
	if (i > 0 && work_ctm[i].score == INITIAL_SCORE || work_ctm[0].score - work_ctm[i].score > CASE_CAND_DIF_MAX) break;
	copy_ctm(&work_ctm[i], ctm_ptr);
	ellipsis_analysis(tag_ptr, ctm_ptr, 0, ctm_ptr->result_num);
    }

    if (OptAnaphora & OPT_TRAIN) {
	/* すべての格対応付けがない場合は出力しない */
	rnum_check_flag = 0;
	for (i = CASE_CANDIDATE_MAX; i < CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX; i++) {
 	    if (work_ctm[i].score == INITIAL_SCORE) break;
	    if (work_ctm[i].result_num - work_ctm[i].case_result_num > 0) {
		rnum_check_flag = 1;
	 	break; 
	    }
	}
	if (rnum_check_flag) {
	    /* 正解出力を生成 */
	    /* 同一の格に複数正解が付与されている場合、gresult=-2にする */
	    for (j = 0; *ELLIPSIS_CASE_LIST[j]; j++) gresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])] = -1;
	    for (j = 1; j < tag_ptr->mention_mgr.num; j++) {
		mention_ptr = tag_ptr->mention_mgr.mention + j;	    
		if (mention_ptr->type == 'O') {
		    if (gresult[pp_kstr_to_code(mention_ptr->cpp_string)] != -1) {
			gresult[pp_kstr_to_code(mention_ptr->cpp_string)] = -2;
		    }
		    else {
			gresult[pp_kstr_to_code(mention_ptr->cpp_string)] = mention_ptr->entity->num;
		    }
		}
	    }
	    gresult_s[0] = '\0';
	    for (j = 0; *ELLIPSIS_CASE_LIST[j]; j++) {
		if (gresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])] > -1) {
		    sprintf(tmp_string, "%s:%d ", ELLIPSIS_CASE_LIST[j], gresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])]);
		    strcat(gresult_s, tmp_string);
		}
	    }

	    for (i = CASE_CANDIDATE_MAX; i < CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX; i++) {
		if (work_ctm[i].score == INITIAL_SCORE) break;
		
		/* 出力結果を生成 */
		for (j = 0; *ELLIPSIS_CASE_LIST[j]; j++) aresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])] = -1;
		for (j = work_ctm[i].case_result_num; j < work_ctm[i].result_num; j++) {
		    aresult[work_ctm[i].cf_ptr->pp[work_ctm[i].cf_element_num[j]][0]] = 
			(entity_manager.entity + work_ctm[i].entity_num[j])->num;
		}
		aresult_s[0] = '\0';
		for (j = 0; *ELLIPSIS_CASE_LIST[j]; j++) {
		    if (aresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])] > -1) {
			sprintf(tmp_string, "%s:%d ", ELLIPSIS_CASE_LIST[j], aresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])]);
			strcat(aresult_s, tmp_string);
		    }
		}
		
		/* 正解判定 */
		true_flag = 1; /* 全格が正解の場合に1 */
		for (j = 0; *ELLIPSIS_CASE_LIST[j]; j++) {	    
		    if (gresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])] != aresult[pp_kstr_to_code(ELLIPSIS_CASE_LIST[j])]) {
			true_flag = 0;
		    }
		}
		
		/* 不正解、または、既に正解出力をした場合で
		   省略関連の全素性が0（すなわち省略解析対象格がすべて直接格要素によって埋まる）である用例は出力しない */
		if ((!true_flag || !rnum_check_flag) &&
		    work_ctm[i].omit_feature[0][NO_ASSIGNMENT] == INITIAL_SCORE &&
		    work_ctm[i].omit_feature[1][NO_ASSIGNMENT] == INITIAL_SCORE &&
		    work_ctm[i].omit_feature[2][NO_ASSIGNMENT] == INITIAL_SCORE) continue;
		
		/* 素性出力 */
		fprintf(Outfp, ";;< %s>%d FEATURE: %d, %f,", aresult_s, i, true_flag, work_ctm[i].overt_arguments_score);
		for (j = 0; j < ELLIPSIS_CASE_NUM; j++) {
		    for (k = 0; k < O_FEATURE_NUM; k++) {
			(work_ctm[i].omit_feature[j][k] == INITIAL_SCORE) ?
			    fprintf(Outfp, " 0,") : 
			    (work_ctm[i].omit_feature[j][k] == 0.0) ?
			    fprintf(Outfp, " 0,") : 
			    (work_ctm[i].omit_feature[j][k] == 1.0) ?
			    fprintf(Outfp, " 1,") : fprintf(Outfp, " %f,", work_ctm[i].omit_feature[j][k]);
		    }
		}
		fprintf(Outfp, "\n");
		if (true_flag) rnum_check_flag = 0;
	    }		
	    
	    /* 候補ごとの区切りのためのダミー出力 */
	    fprintf(Outfp, ";;<dummy %s> FEATURE: -1,", gresult_s);
	    for (j = 0; j < ELLIPSIS_CASE_NUM * O_FEATURE_NUM + 1; j++) fprintf(Outfp, " 0,");
	    fprintf(Outfp, "\n");
	}
    }

    if (OptExpress == OPT_TD) {
	fprintf(Outfp, "<BR><B>Predicate argument structure ranking</B><BR><NOBR>\n");
	for (i = CASE_CANDIDATE_MAX; i < CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX; i++) {
 	    if (work_ctm[i].score == INITIAL_SCORE) break;
	    fprintf(Outfp, "<B>%d</B>: %.3f 【%s】\n", i - CASE_CANDIDATE_MAX, work_ctm[i].score, work_ctm[i].cf_ptr->cf_id);
	    for (j = 0; j < work_ctm[i].result_num; j++) {
		fprintf(Outfp, " %s:%d:%s",
			pp_code_to_kstr(work_ctm[i].cf_ptr->pp[work_ctm[i].cf_element_num[j]][0]),
			(entity_manager.entity + work_ctm[i].entity_num[j])->num + base_entity_num,
			(cp = strchr((entity_manager.entity + work_ctm[i].entity_num[j])->name, ':')) ?
			cp + 1 : (entity_manager.entity + work_ctm[i].entity_num[j])->name);
	    }
	    for (j = 0; j < work_ctm[i].cf_ptr->element_num; j++) {
		if (!work_ctm[i].filled_element[j] &&
		    match_ellipsis_case(pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]), NULL))
		    fprintf(Outfp, " %s:%s", pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]),
			   (work_ctm[i].cf_ptr->oblig[j]) ? "×" : "-");
	    }
	    fprintf(Outfp, "<BR>\n");
	}
    }
    if (OptDisplay == OPT_DEBUG || OptAnaphora & OPT_PRINT_ENTITY) {
	fprintf(Outfp, ";;\n");
	for (i = CASE_CANDIDATE_MAX; i < CASE_CANDIDATE_MAX + ELLIPSIS_RESULT_MAX; i++) {
 	    if (work_ctm[i].score == INITIAL_SCORE) break;
	    fprintf(Outfp, ";;省略解析候補%d-%d:%2d %.3f %s", 
		   tag_ptr->mention_mgr.mention->sent_num,
		   tag_ptr->num, i - CASE_CANDIDATE_MAX + 1, 
		   work_ctm[i].score, work_ctm[i].cf_ptr->cf_id);
	    for (j = 0; j < work_ctm[i].result_num; j++) {
		fprintf(Outfp, " %s%s:%s%d",
		       (j < work_ctm[i].case_result_num) ? "" : "*",
		       pp_code_to_kstr(work_ctm[i].cf_ptr->pp[work_ctm[i].cf_element_num[j]][0]),
		       (entity_manager.entity + work_ctm[i].entity_num[j])->name,
		       (entity_manager.entity + work_ctm[i].entity_num[j])->num);
	    }
	    for (j = 0; j < work_ctm[i].cf_ptr->element_num; j++) {
		if (!work_ctm[i].filled_element[j] && 
		    match_ellipsis_case(pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]), NULL)) 
		    fprintf(Outfp, " %s:%s", pp_code_to_kstr(work_ctm[i].cf_ptr->pp[j][0]),
			   (work_ctm[i].cf_ptr->oblig[j]) ? "×" : "-");
	    }	    
	    if (tag_ptr->tcf_ptr->cf.type != CF_NOUN) {
		fprintf(Outfp, " (0:%.2f", work_ctm[i].overt_arguments_score);
		for (j = 0; j < ELLIPSIS_CASE_NUM; j++) {
		    fprintf(Outfp, "|%s", ELLIPSIS_CASE_LIST_VERB[j]);
		    for (k = 0; k < O_FEATURE_NUM; k++) {
			if (work_ctm[i].omit_feature[j][k] != INITIAL_SCORE)
			    fprintf(Outfp, ",%d:%.2f", O_FEATURE_NUM * j + k + 1, work_ctm[i].omit_feature[j][k]);
		    }
		}
		fprintf(Outfp, ")");
	    }
	    fprintf(Outfp, "\n");
	}
    }
   
    /* BEST解を保存 */
    if (work_ctm[CASE_CANDIDATE_MAX].score == INITIAL_SCORE) return FALSE;
    copy_ctm(&work_ctm[CASE_CANDIDATE_MAX], tag_ptr->ctm_ptr);
    strcpy(tag_ptr->mention_mgr.cf_id, work_ctm[CASE_CANDIDATE_MAX].cf_ptr->cf_id);
    tag_ptr->mention_mgr.cf_ptr = work_ctm[CASE_CANDIDATE_MAX].cf_ptr;

    /* 格フレームを解放 */
    free(cf_array);

    return TRUE;
}

/*==================================================================*/
    int make_new_entity(TAG_DATA *tag_ptr, MENTION_MGR *mention_mgr)
/*==================================================================*/
{    
    char *cp;
    ENTITY *entity_ptr;

    entity_ptr = entity_manager.entity + entity_manager.num;
    entity_ptr->num = entity_ptr->output_num = entity_manager.num;
    entity_manager.num++;				
    entity_ptr->mention[0] = mention_mgr->mention;
    entity_ptr->mentioned_num = 1;

    /* 先行詞になりやすさ(基本的に文節主辞なら1) */
    entity_ptr->salience_score = 
	(tag_ptr->inum > 0 || /* 文節内最後の基本句でない */
	 !check_feature(tag_ptr->f, "照応詞候補") ||
	 check_feature(tag_ptr->f, "NE内")) ? 0 : 
	((check_feature(tag_ptr->f, "ハ") || check_feature(tag_ptr->f, "モ")) &&
	 !check_feature(tag_ptr->f, "括弧終") ||
	 check_feature(tag_ptr->f, "文末")) ? SALIENCE_THEMA : /* 文末 */
	(check_feature(tag_ptr->f, "読点") && tag_ptr->para_type != PARA_NORMAL ||
	 check_feature(tag_ptr->b_ptr->f, "文頭") ||
	 check_feature(tag_ptr->f, "係:ガ格") ||
	 check_feature(tag_ptr->f, "係:ヲ格")) ? SALIENCE_CANDIDATE : SALIENCE_NORMAL;

    /* ENTITYの名前 */
    if (cp = check_feature(tag_ptr->f, "NE")) {
	strcpy(entity_ptr->name, cp + strlen("NE:"));
    }
    else if (cp = check_feature(tag_ptr->f, "照応詞候補")) {
	strcpy(entity_ptr->name, cp + strlen("照応詞候補:"));
    }
    else {
	strcpy(entity_ptr->name, tag_ptr->head_ptr->Goi2);
    }

    mention_mgr->mention->entity = entity_ptr;	    
    mention_mgr->mention->explicit_mention = NULL;    
    strcpy(mention_mgr->mention->cpp_string, "＊");
    if ((cp = check_feature(tag_ptr->f, "係"))) {
	strcpy(mention_mgr->mention->spp_string, cp + strlen("係:"));
    }
    else {
	strcpy(mention_mgr->mention->spp_string, "＊");
    }
    mention_mgr->mention->type = 'S'; /* 自分自身 */   
}

/*==================================================================*/
	    int make_context_structure(SENTENCE_DATA *sp)
/*==================================================================*/
{
    /* 共参照解析結果を読み込み、省略解析を行い文の構造を構築する */
    int i, j, k, check_result;
    char *cp;
    TAG_DATA *tag_ptr;
    CF_PRED_MGR *cpm_ptr;
    MENTION_MGR *mention_mgr;
   
    /* 省略以外のMENTIONの処理 */
    for (i = 0; i < sp->Tag_num; i++) { /* 解析文のタグ単位:i番目のタグについて */
	tag_ptr = substance_tag_ptr(sp->tag_data + i);

	/* 自分自身(MENTION)を生成 */       
	mention_mgr = &(tag_ptr->mention_mgr);
	mention_mgr->mention->tag_num = i;
	mention_mgr->mention->sent_num = sp->Sen_num;
	mention_mgr->mention->tag_ptr = tag_ptr;
	mention_mgr->mention->entity = NULL;
	mention_mgr->mention->explicit_mention = NULL;
	mention_mgr->mention->salience_score = 0;
	mention_mgr->num = 1;

	/* 入力から正解を読み込む場合 */
	if (OptReadFeature & OPT_COREFER) {
	    if (cp = check_feature(tag_ptr->f, "格解析結果")) {		
		for (cp = strchr(cp + strlen("格解析結果:"), ':') + 1; *cp; cp++) {
		    if (*cp == ':' || *cp == ';') {
			if (read_one_annotation(sp, tag_ptr, cp + 1, TRUE))
			    assign_cfeature(&(tag_ptr->f), "共参照", FALSE);
		    }
		}
	    }
	}
	/* 自動解析の場合 */
	else if (cp = check_feature(tag_ptr->f, "Ｔ共参照")) {
	    read_one_annotation(sp, tag_ptr, cp + strlen("Ｔ共参照:"), TRUE);
	}

	/* 新しいENTITYである場合 */	
	if (!mention_mgr->mention->entity) {
	    make_new_entity(tag_ptr, mention_mgr);
	}
    }

    /* 省略解析を行う場合 */
    for (i = sp->Tag_num - 1; i >= 0; i--) { /* 解析文のタグ単位:i番目のタグについて */
	tag_ptr = substance_tag_ptr(sp->tag_data + i);
	check_result = check_analyze_tag(tag_ptr);
	if (!check_result) continue;	    
	
	/* 解析対象格の設定 */
	ELLIPSIS_CASE_LIST = (check_result == CF_PRED) ?
	    ELLIPSIS_CASE_LIST_VERB : ELLIPSIS_CASE_LIST_NOUN;

	/* 省略のMENTIONの処理 */
	/* 入力から正解を読み込む場合 */
	if (OptAnaphora & OPT_TRAIN) {
	    for (j = 0; j < entity_manager.num; j++) entity_manager.entity[j].salience_mem = 0;
	}
	if (check_result == CF_PRED && (OptReadFeature & OPT_ELLIPSIS) || 
	    check_result == CF_NOUN && (OptReadFeature & OPT_REL_NOUN)) {

	    /* この時点での各EntityのSALIENCE出力 */
	    if (OptDisplay == OPT_DEBUG || OptAnaphora & OPT_PRINT_ENTITY) {
		fprintf(Outfp, ";;SALIENCE-%d-%d", sp->Sen_num, i);
		for (j = 0; j < entity_manager.num; j++) {
		    fprintf(Outfp, ":%.3f", (entity_manager.entity + j)->salience_score);
		}
		fprintf(Outfp, "\n");
	    }
	    
	    /* featureから格解析結果を取得 */
	    if (cp = check_feature(tag_ptr->f, "格解析結果")) {		
		
		/* 共参照関係にある表現は格解析結果を取得しない */
		if (check_feature(tag_ptr->f, "体言") &&
		    (strstr(cp, "=/") || strstr(cp, "=構/") || strstr(cp, "=役/"))) {
		    assign_cfeature(&(tag_ptr->f), "共参照", FALSE);
		    continue;
		}
		
		for (cp = strchr(cp + strlen("格解析結果:"), ':') + 1; *cp; cp++) {
		    if (*cp == ':' || *cp == ';') {
			read_one_annotation(sp, tag_ptr, cp + 1, FALSE);
		    }
		}
	    }
	}	

	/* 省略解析を行う場合、または、素性を出力する場合 */
	if (check_result == CF_PRED && tag_ptr->cf_ptr && !(OptReadFeature & OPT_ELLIPSIS) ||
	    check_result == CF_NOUN && tag_ptr->ncf_ptr && !(OptReadFeature & OPT_REL_NOUN) ||
	    (OptAnaphora & OPT_TRAIN)) {
  
	    assign_cfeature(&(tag_ptr->f), "Ｔ省略解析", FALSE);

	    /* cpm_ptrの作成(基本的にはtcf_ptrを使用するが、set_tag_case_frameの呼び出し、および、
	       get_ex_probability_with_para内でtcf_ptr->cf.pred_b_ptr->cpm_ptrとして使用している) */
	    cpm_ptr = (CF_PRED_MGR *)malloc_data(sizeof(CF_PRED_MGR), "make_context_structure: cpm_ptr");
	    init_case_frame(&(cpm_ptr->cf));
	    cpm_ptr->pred_b_ptr = tag_ptr;

	    /* tag_ptr->tcf_ptrを作成 */
	    tag_ptr->tcf_ptr = (TAG_CASE_FRAME *)malloc_data(sizeof(TAG_CASE_FRAME), "make_context_structure: tcf_ptr");
	    set_tag_case_frame(sp, tag_ptr, cpm_ptr);
		
	    /* 位置カテゴリの生成 */	    
	    mark_loc_category(sp, tag_ptr);
	    mark_nearest_case(sp, tag_ptr);
		
	    /* この時点での各EntityのSALIENCE出力 */
	    if (OptDisplay == OPT_DEBUG || OptAnaphora & OPT_PRINT_ENTITY) {
		fprintf(Outfp, ";;SALIENCE-%d-%d", sp->Sen_num, i);
		for (j = 0; j < entity_manager.num; j++) {
		    fprintf(Outfp, ":%.3f", (entity_manager.entity + j)->salience_score);
		}
		fprintf(Outfp, "\n");
	    }
	    if (OptExpress == OPT_TD) {
		fprintf(Outfp, "%%%% LABEL=%d_%d_3\n", sp->Sen_num + base_sentence_num, i + 2);
		fprintf(Outfp, "<B>Salience score ranking</B><BR>\n");
		k = 0;
		for (j = 0; j < entity_manager.num; j++) {
		    if ((entity_manager.entity + j)->salience_score > SALIENCE_THRESHOLD / 2) {
			struct_for_qsort[k].score = (entity_manager.entity + j)->salience_score;
			sprintf(struct_for_qsort[k++].string, "<B>%d</B>:%s:", j + base_entity_num, 
				(cp = strchr((entity_manager.entity + j)->name, ':')) ? 
				cp + 1 : (entity_manager.entity + j)->name);
		    }			
		}
		/* score順にソートして出力 */
		qsort(struct_for_qsort, k, sizeof(STRUCT_FOR_QSORT), cmp_sfq);
		for (j = 0; j < k; j++) {
		    fprintf(Outfp, "%s%.3f%s \n", struct_for_qsort[j].string, struct_for_qsort[j].score,
			    (j == k - 1) ? "" : ",");
		}
		fprintf(Outfp, "<BR>\n");
	    }
		
	    /* 省略解析メイン */
	    tag_ptr->ctm_ptr = (CF_TAG_MGR *)malloc_data(sizeof(CF_TAG_MGR), "make_context_structure: ctm_ptr");
	    tag_ptr->ctm_ptr->score = INITIAL_SCORE;
	    ellipsis_analysis_main(tag_ptr, check_result);
				
	    if (!(OptAnaphora & OPT_TRAIN) &&
		tag_ptr->ctm_ptr->score != INITIAL_SCORE) {
		if (OPT_EXPAND_PARA) expand_result_to_parallel_entity(tag_ptr); /* 並列要素を展開する */
		anaphora_result_to_entity(tag_ptr); /* 解析結果をENTITYと関連付ける */
	    }
	    if (OptAnaphora & OPT_TRAIN) {
		for (j = 0; j < entity_manager.num; j++) 
		    entity_manager.entity[j].salience_score += entity_manager.entity[j].salience_mem;
	    }

	    /* メモリを解放 */
	    free(tag_ptr->ctm_ptr);
	    free(tag_ptr->tcf_ptr);
	    clear_case_frame(&(cpm_ptr->cf));
	    free(tag_ptr->cpm_ptr);
	}
    }
}

/*==================================================================*/
		   void print_entities(int sen_num)
/*==================================================================*/
{
    int i, j;
    char *cp;
    MENTION *mention_ptr;
    ENTITY *entity_ptr;
    FEATURE *fp;
    MRPH_DATA m;

    fprintf(Outfp, ";;\n;;SENTENCE %d\n", sen_num + base_sentence_num); 
    for (i = 0; i < entity_manager.num; i++) {
	entity_ptr = entity_manager.entity + i;
	if (entity_ptr->salience_score < 0.01 && entity_ptr->mentioned_num < 2 ||
	    entity_ptr->salience_score == 0) continue;

	fprintf(Outfp, ";; ENTITY %d [ %s ] %f {\n", entity_ptr->output_num + base_entity_num, 
	       (cp = strchr(entity_ptr->name, ':')) ? cp + 1 : entity_ptr->name,
	       entity_ptr->salience_score);
	for (j = 0; j < entity_ptr->mentioned_num; j++) {
	    mention_ptr = entity_ptr->mention[j];
	    fprintf(Outfp, ";;\tMENTION%3d {", j);
	    fprintf(Outfp, " SEN:%3d", mention_ptr->sent_num + base_sentence_num);
	    fprintf(Outfp, " TAG:%3d", mention_ptr->tag_num);
	    fprintf(Outfp, " (%3d)", mention_ptr->tag_ptr->head_ptr->Num);
	    fprintf(Outfp, " CPP: %4s", mention_ptr->cpp_string);
	    fprintf(Outfp, " SPP: %4s", mention_ptr->spp_string);
	    fprintf(Outfp, " TYPE: %c", mention_ptr->type);
	    fprintf(Outfp, " SS: %.3f", mention_ptr->salience_score);
	    fprintf(Outfp, " WORD: %s", mention_ptr->tag_ptr->head_ptr->Goi2);
	    fprintf(Outfp, " }\n");
	}
	fprintf(Outfp, ";; }\n;;\n");
    }
}

/*==================================================================*/
	    void assign_anaphora_result(SENTENCE_DATA *sp)
/*==================================================================*/
{
    /* 照応解析結果を基本句のfeatureに付与 */
    int i, j, count;
    char buf[DATA_LEN], tmp[IMI_MAX], *cp;
    MENTION *mention_ptr;
    TAG_DATA *tag_ptr;
	     
    for (i = 0; i < sp->Tag_num; i++) {
	tag_ptr = substance_tag_ptr(sp->tag_data + i);
	sprintf(buf, "EID:%d", tag_ptr->mention_mgr.mention->entity->num + base_entity_num);
	assign_cfeature(&(tag_ptr->f), buf, FALSE);
	if (!check_feature(tag_ptr->f, "Ｔ省略解析") ||
	    !((OptReadFeature & OPT_ELLIPSIS) || tag_ptr->mention_mgr.cf_id[0])) continue;

	if (OptDisplay == OPT_SIMPLE && OptExpress != OPT_TD) {
	    sprintf(buf, "項構造:");		    
	}
	else {
	    sprintf(buf, "項構造:%s:", (OptReadFeature & OPT_ELLIPSIS) ? "?" : tag_ptr->mention_mgr.cf_id);
	}		    
	for (j = 1; j < tag_ptr->mention_mgr.num; j++) {
	    mention_ptr = tag_ptr->mention_mgr.mention + j;
	    
	    if (mention_ptr->type == 'N' || mention_ptr->type == 'C' ||
		mention_ptr->type == 'O' || mention_ptr->type == 'D') {
		if (OptDisplay == OPT_SIMPLE && OptExpress != OPT_TD) {
		    sprintf(tmp, "%s/%s/%d;", mention_ptr->cpp_string,
			    (cp = strchr(mention_ptr->entity->name, ':')) ? cp + 1 : mention_ptr->entity->name,
			    mention_ptr->entity->num + base_entity_num);
		}
		else {
		    sprintf(tmp, "%s/%c/%s/%d;", mention_ptr->cpp_string, mention_ptr->type, 
			    (cp = strchr(mention_ptr->entity->name, ':')) ? cp + 1 : mention_ptr->entity->name,
			    mention_ptr->entity->num + base_entity_num);
		}
		strcat(buf, tmp);
	    }
	}
	buf[strlen(buf) - 1] = '\0'; /* 末尾の';'、':'を削除 */
	if (strcmp(buf, "項構造")) { /* simpleオプションで項が存在しない場合は項構造情報を非表示にする*/
	    assign_cfeature(&(tag_ptr->f), buf, FALSE);
	}
    }
}

/*==================================================================*/
			 void decay_entity()
/*==================================================================*/
{
    /* ENTITYの活性値を減衰させる */
    int i;

    for (i = 0; i < entity_manager.num; i++) {
	entity_manager.entity[i].salience_score *= SALIENCE_DECAY_RATE;
    }
}

/*==================================================================*/
	      void anaphora_analysis(SENTENCE_DATA *sp)
/*==================================================================*/
{
    if (ModifyWeight[0]) {
	case_feature_weight[0][ASSIGNED] += ModifyWeight[0];
	case_feature_weight[1][ASSIGNED] += ModifyWeight[1];
	case_feature_weight[2][ASSIGNED] += ModifyWeight[2];
	ModifyWeight[0] = 0;
    }
    decay_entity();
    make_context_structure(sentence_data + sp->Sen_num - 1);
    assign_anaphora_result(sentence_data + sp->Sen_num - 1);
    if (OptAnaphora & OPT_PRINT_ENTITY) print_entities(sp->Sen_num);
}

/*==================================================================*/
		void ClearSentence(SENTENCE_DATA *s)
/*==================================================================*/
{
    int i;

    for (i = 0; i < s->Mrph_num; i++) clear_feature(&(s->mrph_data[i].f));
    free(s->mrph_data);
    for (i = 0; i < s->Bnst_num; i++) clear_feature(&(s->bnst_data[i].f));
    free(s->bnst_data);
    for (i = 0; i < s->Tag_num; i++) clear_feature(&(s->tag_data[i].f));
    free(s->tag_data);
    free(s->para_data);
    free(s->para_manager);
    free(s->Comment);
    if (s->cpm)	free(s->cpm);
    if (s->cf) free(s->cf);
    if (s->KNPSID) free(s->KNPSID);
    if (s->Best_mgr) {clear_mgr_cf(s); free(s->Best_mgr);}
}

/*==================================================================*/
		void ClearSentences(SENTENCE_DATA *sp)
/*==================================================================*/
{
    int i;

    for (i = 0; i < sp->Sen_num - 1; i++) {
	if (OptArticle) print_result(sentence_data+i, 1, 1);
	ClearSentence(sentence_data+i);
    }
    sp->Sen_num = 1;
    corefer_id = 0;
}

/*==================================================================*/
	  SENTENCE_DATA *PreserveSentence(SENTENCE_DATA *sp)
/*==================================================================*/
{
    /* 文解析結果の保持 */
    int i, j;
    SENTENCE_DATA *sp_new;

    /* 一時的措置 */
    if (sp->Sen_num > SENTENCE_MAX) {
	fprintf(stderr, "Sentence buffer overflowed!\n");
	ClearSentences(sp);
    }

    sp_new = sentence_data + sp->Sen_num - 1;

    sp_new->available = sp->available;
    sp_new->Sen_num = sp->Sen_num;
    if (sp->Comment) {
	sp_new->Comment = strdup(sp->Comment);
    }

    sp_new->Mrph_num = sp->Mrph_num;
    sp_new->mrph_data = (MRPH_DATA *)malloc_data(sizeof(MRPH_DATA)*sp->Mrph_num, "MRPH DATA");
    for (i = 0; i < sp->Mrph_num; i++) {
	sp_new->mrph_data[i] = sp->mrph_data[i];
	/* featureポインタはそのままコピーして、clear_all_featuresの際にspからのリンクを切る */
    }

    sp_new->Bnst_num = sp->Bnst_num;
    sp_new->New_Bnst_num = sp->New_Bnst_num;
    sp_new->bnst_data = (BNST_DATA *)malloc_data(sizeof(BNST_DATA)*(sp->Bnst_num + sp->New_Bnst_num), "BNST DATA");
    for (i = 0; i < sp->Bnst_num + sp->New_Bnst_num; i++) {

	sp_new->bnst_data[i] = sp->bnst_data[i]; /* ここでbnst_dataをコピー */
	/* featureポインタはそのままコピーして、clear_all_featuresの際にspからのリンクを切る */

	/* SENTENCE_DATA 型 の sp は, MRPH_DATA をメンバとして持っている    */
	/* 同じく sp のメンバである BNST_DATA は MRPH_DATA をメンバとして持っている */
	/* そのため、単に BNST_DATA をコピーしただけだと、BNST_DATA 内の MRPH_DATA */
        /* は, sp のほうの MRPH_DATA を差したままコピーされるので、以下でポインタアドレスのずれを補正 */

	/* 単にコピーしたままだと、sp_new->bnst_data[i] の mrph_data は, sp のデータを指してしまう */
	/* 元のデータ構造を保つためには、自分自身(sp_new)のデータ(メンバ)を指すように修正 */
	sp_new->bnst_data[i].mrph_ptr = sp_new->mrph_data + (sp->bnst_data[i].mrph_ptr - sp->mrph_data);
	sp_new->bnst_data[i].head_ptr = sp_new->mrph_data + (sp->bnst_data[i].head_ptr - sp->mrph_data);

	if (sp->bnst_data[i].parent)
	    sp_new->bnst_data[i].parent = sp_new->bnst_data + (sp->bnst_data[i].parent - sp->bnst_data);
	for (j = 0; sp_new->bnst_data[i].child[j]; j++) {
	    sp_new->bnst_data[i].child[j] = sp_new->bnst_data + (sp->bnst_data[i].child[j] - sp->bnst_data);
	}
	if (sp->bnst_data[i].pred_b_ptr) {
	    sp_new->bnst_data[i].pred_b_ptr = sp_new->bnst_data + (sp->bnst_data[i].pred_b_ptr - sp->bnst_data);
	}
    }

    sp_new->Tag_num = sp->Tag_num;
    sp_new->New_Tag_num = sp->New_Tag_num;
    sp_new->tag_data = (TAG_DATA *)malloc_data(sizeof(TAG_DATA)*(sp->Tag_num + sp->New_Tag_num), "TAG DATA");
    for (i = 0; i < sp->Tag_num + sp->New_Tag_num; i++) {
	sp_new->tag_data[i] = sp->tag_data[i]; /* ここでtag_dataをコピー */
	/* featureポインタはそのままコピーして、clear_all_featuresの際にspからのリンクを切る */

	sp_new->tag_data[i].mrph_ptr = sp_new->mrph_data + (sp->tag_data[i].mrph_ptr - sp->mrph_data);
	if (sp->tag_data[i].settou_ptr)
	    sp_new->tag_data[i].settou_ptr = sp_new->mrph_data + (sp->tag_data[i].settou_ptr - sp->mrph_data);
	sp_new->tag_data[i].jiritu_ptr = sp_new->mrph_data + (sp->tag_data[i].jiritu_ptr - sp->mrph_data);
	if (sp->tag_data[i].fuzoku_ptr)
	sp_new->tag_data[i].fuzoku_ptr = sp_new->mrph_data + (sp->tag_data[i].fuzoku_ptr - sp->mrph_data);
	sp_new->tag_data[i].head_ptr = sp_new->mrph_data + (sp->tag_data[i].head_ptr - sp->mrph_data);
	if (sp->tag_data[i].parent)
	    sp_new->tag_data[i].parent = sp_new->tag_data + (sp->tag_data[i].parent - sp->tag_data);
	for (j = 0; sp_new->tag_data[i].child[j]; j++) {
	    sp_new->tag_data[i].child[j] = sp_new->tag_data + (sp->tag_data[i].child[j] - sp->tag_data);
	}
	if (sp->tag_data[i].pred_b_ptr) {
	    sp_new->tag_data[i].pred_b_ptr = sp_new->tag_data + (sp->tag_data[i].pred_b_ptr - sp->tag_data);
	}
	if (sp->tag_data[i].next) {
	    sp_new->tag_data[i].next = sp_new->tag_data + (sp->tag_data[i].next - sp->tag_data);
	}
	sp_new->tag_data[i].b_ptr = sp_new->bnst_data + (sp->tag_data[i].b_ptr - sp->bnst_data);
    }

    for (i = 0; i < sp->Bnst_num + sp->New_Bnst_num; i++) {
	if (sp->bnst_data[i].tag_ptr) 
	    sp_new->bnst_data[i].tag_ptr = sp_new->tag_data + (sp->bnst_data[i].tag_ptr - sp->tag_data);
    }

    if (sp->KNPSID) sp_new->KNPSID = strdup(sp->KNPSID);
    else sp_new->KNPSID = NULL;

    sp_new->para_data = (PARA_DATA *)malloc_data(sizeof(PARA_DATA)*sp->Para_num, "PARA DATA");
    for (i = 0; i < sp->Para_num; i++) {
	sp_new->para_data[i] = sp->para_data[i];
	sp_new->para_data[i].manager_ptr += sp_new->para_manager - sp->para_manager;
    }

    sp_new->para_manager = (PARA_MANAGER *)malloc_data(sizeof(PARA_MANAGER)*sp->Para_M_num, "PARA MANAGER");
    for (i = 0; i < sp->Para_M_num; i++) {
	sp_new->para_manager[i] = sp->para_manager[i];
	sp_new->para_manager[i].parent += sp_new->para_manager - sp->para_manager;
	for (j = 0; j < sp_new->para_manager[i].child_num; j++) {
	    sp_new->para_manager[i].child[j] += sp_new->para_manager - sp->para_manager;
	}
	sp_new->para_manager[i].bnst_ptr += sp_new->bnst_data - sp->bnst_data;
    }
    sp_new->cpm = NULL;
    sp_new->cf = NULL;
}
