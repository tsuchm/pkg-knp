/*====================================================================

			      PATHS

                                               S.Kurohashi 94. 8.25

    $Id: path.h,v 1.48 2011/11/25 01:25:52 kawahara Exp $
====================================================================*/

#define BGH_DB_NAME		"scode/bgh/bgh.db"
#define SM2BGHCODE_DB_NAME	"scode/bgh/sm2code.db"
#define SM_DB_NAME		"scode/ntt/word2code.db"
#define SM2CODE_DB_NAME		"scode/ntt/sm2code.db"
#define CODE2SM_DB_NAME		"scode/ntt/code2sm.db"
#define SMP2SMG_DB_NAME		"scode/ntt/smp2smg.db"
#define SCASE_DB_NAME		"gcf/scase.db"
#define CF_DB_NAME		"ebcf/cf.db"
#define CF_DAT_NAME		"ebcf/cf.dat"
#define CF_SIM_DB_NAME		"ebcf/cfsim.db"
#define CF_CASE_DB_NAME		"ebcf/cfcase.db"
#define CF_EX_DB_NAME		"ebcf/cfex.db"
#define CASE_DB_NAME		"ebcf/case.db"
#define CFP_DB_NAME		"ebcf/cfp.db"
#define RENYOU_DB_NAME		"ebcf/renyou.db"
#define ADVERB_DB_NAME		"ebcf/adverb.db"
#define PARA_DB_NAME		"ebcf/para.db"
#define NOUN_CO_DB_NAME		"ebcf/noun_co.db"
#define CF_NOUN_DB_NAME		"ebcf/noun.db"
#define CF_NOUN_DAT_NAME	"ebcf/noun.dat"
#define MRPH2ID_DB_NAME		"ebcf/mrph2id.db"
#define SM_ADD_DB_NAME		"scode/ntt/word2code_add.db"
#define SM_DEL_DB_NAME		"scode/ntt/word2code_del.db"
#define EVENT_DB_NAME		"event/event.db"
#define AUTO_DIC_DB_NAME	"auto/auto.db"
#define SYONONYM_DIC_DB_NAME	"synonym/synonym.db"
#define NE_CRF_MODEL_NAME	"crf.model"
#define DISTSIM_DB_NAME		"distsim/mi.db"
