#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef STDC_HEADERS
#include <stdio.h>
#endif

#ifdef HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef HAVE_SETJMP_H
#include <setjmp.h>
#endif

#ifdef HAVE_MATH_H
#include <math.h>
#endif

#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_CTYPE_H
#include <ctype.h>
#endif

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

#include <crfpp.h>
#include <iostream>
#include "crf.h"

#define NE_MODEL_NUMBER	33 /* system/const.hの定義と揃える */

int NE_TABLE[NE_MODEL_NUMBER]; /* KNP内部のクラス番号とCRF++の内部クラス番号の変換テーブル */
char *CRFFileNE;
CRFPP::Tagger *tagger;

int init_crf_for_NE() {
    int i, j;
    char argc = 5, *argv[5];

    /* make an argument of CRFPP::createTagger */
    argv[0] = const_cast<char*>("knp");
    argv[1] = const_cast<char*>("-m");
    argv[2] = CRFFileNE;
    argv[3] = const_cast<char*>("-v"); /* -v 2: access marginal probablilities */
    argv[4] = const_cast<char*>("1");
    
    tagger = CRFPP::createTagger(argc, argv);
    if (!tagger) {
	fprintf(stderr, ";; CRF initialization error (%s is corrupted?).\n", CRFFileNE);
	exit(1);
    }

    /* KNP内部のクラス番号とCRF++の内部クラス番号の変換テーブルを作成 */
    /* CRF++に対応するクラスがない場合(学習データにない場合)は-1を代入 */
    for (j = 0; j < NE_MODEL_NUMBER; j++) NE_TABLE[j] = -1;      
    for (i = 0; i < tagger->ysize(); i++) {
	if (atoi(tagger->yname(i))) {
	    j =  atoi(tagger->yname(i)) - 100;
	    if (0 <= j && j < NE_MODEL_NUMBER) {
		NE_TABLE[j] = i;
	    }
	}
    }
    
    return 0;
}

void crf_add(char *line) {
    tagger->add(line);
}

void clear_crf() {
    tagger->clear();
}

void crf_parse() {
    if (!tagger->parse()) {
	fprintf(stderr, ";; CRF parse error.\n");
	exit(1);
    }
}

void get_crf_prob(int i, int j, double *prob) {
    if (NE_TABLE[j] < 0) { /* CRF++に対応するクラスがない場合 */
	*prob = 0.0000001;
    }
    else {
	*prob = tagger->prob(i, NE_TABLE[j]);
    }
}
